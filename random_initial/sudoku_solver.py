import tensorflow as tf
import tensorflow.contrib.slim as slim
import numpy as np

def CNN(input_feature, is_training, keep_prob):
	input_feature = tf.reshape(input_feature, [-1, 81, 9, 1])
	weights_regularizer = slim.l2_regularizer(1e-5)
	batch_norm = slim.batch_norm

	batch_norm_params = {'is_training':is_training,'updates_collections':tf.GraphKeys.UPDATE_OPS,'decay':0.9,'epsilon':0.00001}

	############## CNN ###############
	x = slim.conv2d(scope='encoder/conv1',inputs=input_feature,num_outputs=16, kernel_size=[3,3],stride=[1,1],
	 	normalizer_fn=slim.batch_norm, normalizer_params=batch_norm_params,weights_regularizer = weights_regularizer)
	print(x.shape)
	x = slim.max_pool2d(scope='encoder/pool1',inputs=x,kernel_size=[2,2],stride=[2,2],padding='SAME')
	print(x.shape)
	x = slim.conv2d(scope='encoder/conv2',inputs=x,num_outputs=32, kernel_size=[3,3],stride=[1,1],
		normalizer_fn=slim.batch_norm,normalizer_params=batch_norm_params,weights_regularizer = weights_regularizer)		
	print(x.shape)
	x = slim.max_pool2d(scope='encoder/pool2',inputs=x,kernel_size=[2,2],stride=[2,2],padding='SAME')
	print(x.shape)
	x = slim.conv2d(scope='encoder/conv3',inputs=x,num_outputs=64,kernel_size=[3,3],stride=[1,1],
		normalizer_fn=slim.batch_norm,normalizer_params=batch_norm_params,weights_regularizer = weights_regularizer)		
	print(x.shape)
	x = slim.max_pool2d(scope='encoder/pool3',inputs=x,kernel_size=[2,2],stride=[2,2],padding='SAME')
	print(x.shape)


	x = tf.reshape(x,[-1,11*2*64])
	x = slim.fully_connected(x, 1024, weights_regularizer=weights_regularizer, scope='encoder/fc_1')
	x = slim.fully_connected(x, 512, weights_regularizer=weights_regularizer, scope='encoder/fc_2')
	#x = slim.dropout(x, keep_prob=keep_prob, is_training=is_training)
	return x

def compute_alldiff_loss(logits, edges, keep_prob, n):
    """
    :param logits: (bs, 81, 9)
    :param edges: (27, 9)
    """
    P = tf.nn.softmax(logits, axis=2)
    all_diffs = tf.gather(P, edges, axis=1) # bs*27*9*9
    all_diffs_T = tf.transpose(all_diffs, perm=[0,1,3,2]) 
    innerProduct = tf.matmul(all_diffs, all_diffs_T) # bs*27*9*9
    loss1 = tf.square(innerProduct - tf.eye(tf.shape(innerProduct)[3]))
    loss1 = slim.dropout(loss1, keep_prob = keep_prob) # [0, 1) 
    loss1 = tf.reduce_mean(loss1) #minimize xxx


    all_diffs_D = tf.reduce_mean(all_diffs, axis=2) # bs*27*9 the avg distribution over each all_diff constraints
    all_diff_H = tf.reduce_sum(-all_diffs_D * tf.log(all_diffs_D), axis=2) #bs*27 : 0 ~ ln(9)
    loss2 = -slim.dropout(all_diff_H, keep_prob = keep_prob) # [-ln(n), 0] 
    loss2 = tf.reduce_mean(loss2)/np.log(n) #maximize all_diff entropy 

    all_diff_loss = loss2 


    H = tf.reduce_sum(-P * tf.log(P), axis=2)
    entropy = slim.dropout(H, keep_prob = 1.0) #[0, ln(n)]
    entropy = tf.reduce_mean(entropy)/np.log(n) #minimize cell entropy 

    return all_diff_loss, entropy
    #return loss1 + loss2 + loss3 

def sudoku_alldiff(size): #produce all diff sets
    size2 = size*size
    idx = np.arange(size2).reshape(size, size)
    all_diff = []
    for i in range(size):
        all_diff.append(idx[:,i])

    for i in range(size):
        all_diff.append(idx[i,:])

    sqr_size = int(np.sqrt(size))
    for i in range(sqr_size):
        for j in range(sqr_size):
            all_diff.append(idx[i*sqr_size:(i+1)*sqr_size, j*sqr_size:(j+1)*sqr_size].reshape(-1))
    all_diff = np.asarray(all_diff, dtype="int32")
    #print(all_diff.shape)
    return all_diff #(1620, 2)

def PuzzleAcc(Qs, indices, As, size):
    S = 0
    unsolved = []
    #print(Qs.shape)
    #print(As.shape)
    for idx in range(Qs.shape[0]):
        Q = Qs[idx].reshape((size,size))
        A = As[idx].reshape((size,size))
        n_err = 0
        err_type = []
        for i in range(size):
            for j in range(size):
                if (Q[i][j]!=0 and Q[i][j]!=A[i][j]):
                    n_err += 1
                    err_type.append("same: %d %d"%(i,j))

        for i in range(size):
            if (len(list(set(A[i,:])))!=size):
                n_err += 1
                err_type.append("row: %d"%(i))

        for i in range(size):    
            if (len(list(set(A[:,i])))!=size):
                n_err += 1 
                err_type.append("col: %d"%(i))

        sqr_size = int(np.sqrt(size))
        for i in range(sqr_size):
            for j in range(sqr_size):
                tmp = []
                for k in range(i*sqr_size, i*sqr_size+sqr_size):
                    for l in range(j*sqr_size, j*sqr_size+sqr_size):
                        tmp.append(A[k][l])
                if (len(list(set(tmp)))!=size):
                    n_err += 1
                    err_type.append("box: %d %d"%(i,j))
        print(Q)
        print(A)
        print(n_err, err_type)
        S+= int(n_err == 0)
        if (n_err !=0):
            #print(indices.shape, idx)
            unsolved.append(indices[idx])

    return S*1.0/Qs.shape[0], unsolved

def solve(Q, indices, n = 1000, bs =1, size=9):
    alldiff_constraints = sudoku_alldiff(size)
    weights_regularizer=slim.l2_regularizer(1e-4)

    size2 = size*size 
    label = np.greater(Q, 0).astype(int).reshape(bs, size2).astype(np.float32)
    #print(label)
    ans = np.maximum(Q.reshape(bs, size2) - 1, 0).astype(int)
    #print(ans)

    A = tf.placeholder(dtype=tf.int32,shape=[None, size2], name='A')
    L = tf.placeholder(dtype=tf.float32,shape=[None, size2], name='L')

    W = tf.Variable(np.random.randn(bs, size2, size), "w", dtype=tf.float32)

    x = tf.reshape(W, (bs, -1))
    x = slim.fully_connected(x, 256, weights_regularizer=weights_regularizer)
    x = slim.fully_connected(x, 1024, weights_regularizer=weights_regularizer)
    #x = slim.fully_connected(x, 1024, weights_regularizer=weights_regularizer)
    #x = tf.reshape(W, (bs, size2, size, 1))
    #x = CNN(W, True, 0.5)
    x = slim.fully_connected(x, size2*size, weights_regularizer=weights_regularizer, activation_fn=None, scope="softmax")
    #with tf.variable_scope("softmax", reuse=True):
    #    last_layer = tf.get_variable('weights')
    #x = W
    W2 = tf.Variable(np.random.randn(bs, size2, size), "w2", dtype=tf.float32)
    output = tf.reshape(x, (bs, size2, size)) + W2 
    perturb = W2.assign_add(tf.random_normal(W2.shape)*0.01)

    prob = tf.nn.sparse_softmax_cross_entropy_with_logits(labels=A, logits=output)
    nll_loss = tf.reduce_mean(prob*L)

    sudoku = tf.cast(tf.argmax(output, axis=2)+1, tf.int32)
    print("output", output.shape)
    #entropy_loss = compute_entropy_loss(output)
    alldiff_loss, entropy_loss = compute_alldiff_loss(output, alldiff_constraints, keep_prob=0.5, n = size)

    total_loss = alldiff_loss + nll_loss


    optimizer_loss = alldiff_loss + 0.01*entropy_loss +  nll_loss
  

    session_config = tf.ConfigProto(allow_soft_placement=True)
    session_config.gpu_options.allow_growth = True 

    sess = tf.Session(config=session_config)

    global_step = tf.Variable(initial_value=0, trainable=False)
    optimizer = tf.train.AdamOptimizer(learning_rate= 0.001)
    train_op = optimizer.minimize(optimizer_loss, global_step = global_step)

    sess.run(tf.global_variables_initializer())
    feed_dict={}
    feed_dict[A] = ans
    feed_dict[L] = label

    best_puzzleAcc = 0
    for i in range(n):
        res = sess.run([total_loss, alldiff_loss, entropy_loss, nll_loss, train_op, perturb, sudoku], feed_dict)
        print("iter %d total_loss = %.6f alldiff_loss = %.6f entropy_loss = %.6f nll_loss = %.6f" %(i, res[0], res[1], res[2], res[3]))

        solu = res[-1].reshape(-1, size, size)
        puzzleAcc, unsolved = PuzzleAcc(Q, indices, solu, size)

        print("puzzleAcc = %.6f"%puzzleAcc)
        best_puzzleAcc = puzzleAcc
        if (puzzleAcc == 1):
            break
            return 1
    return best_puzzleAcc, unsolved


def get_puzzle(A, n, size):
    A = A[0]
    indices = []
    for i in range(size):
        for j in range(size):
            indices.append([i,j])
    np.random.shuffle(indices)
    Q = np.copy(A)
    cnt = 0
    for k in range(size*size):
        i = indices[k][0]
        j = indices[k][1]
        if (cnt==n):
            break
        if (Q[i][j]!=0):
            cnt +=1
            Q[i][j]=0
    return np.asarray([Q])

def get_data(data, indices):
    res = []
    for i in indices:
        res.append(data[i])
    return np.asarray(res)


data  = np.asarray(
       [[[ 5,  0,  0, 13,  2,  0,  0, 14,  0,  0,  0, 15,  4,  0,  0,  0],
       [ 2,  6, 10,  0,  1,  5,  0,  0,  0,  8,  0,  0,  0,  0,  0, 15],
       [ 3,  0,  0,  0,  0,  0,  0, 16,  0,  0,  0,  0,  0,  0, 10, 14],
       [ 4,  8,  0,  0,  0,  0,  0,  0,  0,  6, 10,  0,  0,  5,  0,  0],
       [ 0,  5,  0,  0,  0,  2, 14,  0,  0,  0, 15,  0,  0,  0, 16,  0],
       [ 6,  2,  0,  0,  0,  0, 13,  0,  0,  4, 16,  0,  0,  3, 15,  0],
       [ 7,  0,  0, 11,  0,  0,  0,  0,  5,  1,  0,  9,  6,  0, 14, 10],
       [ 0,  0,  0, 12,  0,  3,  0,  0,  6,  0,  0,  0,  5,  1, 13,  0],
       [ 0,  0,  1,  0, 10,  0,  0,  0, 11,  0,  0,  7, 12,  0,  4,  0],
       [ 0,  0,  2,  6,  9, 13,  1,  0,  0,  0,  0,  0, 11, 15,  0,  0],
       [11,  0,  0,  0, 12,  0,  4,  0,  9,  0,  0,  5,  0,  0,  0,  0],
       [ 0, 16,  0,  0,  0, 15,  3,  0,  0,  0,  2,  0,  0,  0,  1,  0],
       [13,  0,  5,  1,  0,  0,  0,  2, 15, 11,  7,  3,  0, 12,  0,  0],
       [ 0,  0,  0,  0, 13,  0,  0,  0, 16,  0,  0,  0,  0, 11,  0,  0],
       [ 0,  0,  7,  0, 16,  0,  0,  0,  0,  0,  5,  0, 14,  0,  6,  0],
       [ 0, 12,  0,  0,  0, 11,  7,  3,  0,  0,  0,  2,  0,  0,  0,  0]]])

Q2   = np.asarray([[
    [6,9,3,7,8,4,5,1,2],
    [4,8,7,5,1,2,9,3,6],
    [1,2,5,9,6,3,8,7,4],
    [9,3,2,6,5,1,4,8,7],
    [5,6,8,2,4,7,3,9,1],
    [7,4,1,3,9,8,6,2,5],
    [3,1,9,4,7,5,2,6,8],
    [8,5,6,1,2,9,7,4,3],
    [2,7,4,8,3,6,1,5,9]
    ]], dtype="int")


#data = get_puzzle(Q2, 81 , size = 9)
data = np.load("../data/test_data.npy")
if (len(data.shape) == 4):
    data = data[:,0,:,:]
bs = 1
overall_acc = 0
n_batch = 3

indices = np.arange(data.shape[0])
np.random.shuffle(indices)

indices = indices[0:1]
Ns = [30000]
#[100, 300, 1000, 3000, 10000, 30000]
_indices = indices

for (z, _n) in enumerate(Ns): 
    test_data = get_data(data, _indices)
    next_indices = []
    n_batch = int((test_data.shape[0] - 1) / bs) + 1
    print("This is round %d with n_batch = %d"%(z, n_batch))
    for i in range(n_batch):

        Qs = test_data[i*bs:min((i+1)*bs, test_data.shape[0])]
        #print(Qs.shape)
        acc, unsolved = solve(Qs, _indices[i*bs:min((i+1)*bs, test_data.shape[0])], n = _n, bs = Qs.shape[0], size = 9)
        print("epoch %d batch_size = %d acc=%.6f"%(i, Qs.shape[0], acc))
        overall_acc += acc * bs
        next_indices += unsolved

    _indices = np.asarray(next_indices)
    print("%d puzzles are still unsolved!"%_indices.shape[0])
    if (_indices.shape[0] == 0):
        break

print("overall acc = %.6f" % (overall_acc/indices.shape[0]))

