import tensorflow as tf
import tensorflow.contrib.slim as slim
import numpy as np
import os 
def compute_entropy_loss(logits, keep_prob):
    """
    :param logits: (bs, 81, 9)
    """
    P = tf.nn.softmax(logits, axis=2) 
    H = tf.reduce_sum(-P * tf.log(P), axis=2)
    loss = slim.dropout(H, keep_prob = keep_prob)
    return tf.reduce_mean(loss)

def compute_alldiff_loss(logits, edges, sample):
    """
    :param logits: (bs, 81, 9)
    :param edges: (27, 9)
    """
    P = tf.nn.softmax(logits, axis=2)

    all_diffs = tf.gather(P, edges, axis=1) # bs*27*9*9
    all_diffs_P = tf.reduce_mean(all_diffs, axis=2)
    H = tf.reduce_sum(-all_diffs_P * tf.log(all_diffs_P), axis=2) #bs*27 : 0 ~ ln(9)
    #minH = tf.reduce_min(H, axis = 1)
    #maxH = tf.reduce_max(H, axis = 1)
    #loss = slim.dropout(H, keep_prob = keep_prob)
    #eps = 0.5
    #means = tf.cast(tf.exp(H - maxH)*(1 - 2*eps) + eps, tf.float32) # bs*27 : eps ~ 1 - eps, the larger the less important
    #sample = tf.where(tf.random_uniform(tf.shape(H)) > means, tf.ones_like(H), tf.zeros_like(H)) # bs*27
    #sample = tf.cast(tf.random_uniform(tf.shape(H)), tf.float64) # bs*27
    loss = tf.cast(sample, tf.float64) * H
    #loss = slim.dropout(H, keep_prob = keep_prob)
    return -tf.reduce_mean(loss)

def sudoku_alldiff(): #produce all diff sets
    idx = np.arange(81).reshape(9, 9)
    all_diff = []
    for i in range(9):
        all_diff.append(idx[:,i])

    for i in range(9):
        all_diff.append(idx[i,:])

    for i in range(3):
        for j in range(3):
            all_diff.append(idx[i*3:(i+1)*3, j*3:(j+1)*3].reshape(-1))
    all_diff = np.asarray(all_diff, dtype="int32")
    #print(all_diff.shape)
    return all_diff #(1620, 2)

def print_sudoku(Q):
    for row in Q:
        print("[", end="")
        for x in row[:-1]:
            print("%d,"%x, end="")
        print("%d],"%row[-1])
    print("")

def PuzzleAcc(Qs, As, solus=None):
    S = 0
    #print(Qs.shape)
    #print(As.shape)
    for k in range(Qs.shape[0]):
        Q = Qs[k].reshape((9,9))
        A = As[k].reshape((9,9))
        n_err = 0
        err_type = []
        for i in range(9):
            for j in range(9):
                if (Q[i][j]!=0 and Q[i][j]!=A[i][j]):
                    n_err += 1
                    err_type.append("type-1: %d %d"%(i,j))

        for i in range(9):
            if (len(list(set(A[i,:])))!=9):
                n_err += 1
                err_type.append("type-2: %d"%(i))

        for i in range(9):    
            if (len(list(set(A[:,i])))!=9):
                n_err += 1 
                err_type.append("type-3: %d"%(i))

        for i in range(3):
            for j in range(3):
                tmp = []
                for k in range(i*3, i*3+3):
                    for l in range(j*3, j*3+3):
                        tmp.append(A[k][l])
                if (len(list(set(tmp)))!=9):
                    n_err += 1
                    err_type.append("type-4: %d %d"%(i,j))
        if (n_err > 0 or 1):            
            print_sudoku(Q)
            print_sudoku(A)
            if (solus != None):
                print_sudoku(solus[k].reshape((9,9)))
            print(n_err, err_type)
        S+= int(n_err == 0)
    return S*1.0/Qs.shape[0], n_err

def solve(Q, initial_solu, n = 1000, m = 0, kp = 0.5, n_step = 1):
    bs = 1
    alldiff_constraints = sudoku_alldiff()
    weights_regularizer=slim.l2_regularizer(1e-4)

    label = np.greater(Q, 0).astype(int).reshape(1,81).astype(np.float32)
    print(label)
    ans = np.maximum(Q.reshape(1,81) - 1, 0).astype(int)
    print(ans)

    initial = np.maximum(initial_solu.reshape(1,81) - 1, 0).astype(int)
    print(initial)

    init = tf.placeholder(dtype=tf.int32, shape=[None, 81], name='initial')
    A = tf.placeholder(dtype=tf.int32, shape=[None, 81], name='A')
    L = tf.placeholder(dtype=tf.float64, shape=[None, 81], name='L')
    sample = tf.placeholder(dtype=tf.int32, shape=[None, 27], name='sample')

    keep_prob = tf.placeholder(dtype = tf.float64)
    W = tf.Variable(np.random.randn(bs, 81, 9), "w")
    x = tf.reshape(W, (bs, -1))
    x = slim.fully_connected(x, 1024, weights_regularizer=weights_regularizer)
    x = slim.fully_connected(x, 1024, weights_regularizer=weights_regularizer)
    #x = slim.fully_connected(x, 1024, weights_regularizer=weights_regularizer)
    #x = slim.fully_connected(x, 1024, weights_regularizer=weights_regularizer)
    x = slim.fully_connected(x, 81*9, weights_regularizer=weights_regularizer, activation_fn=None)
    #x = W
    output = tf.reshape(x, (bs, 81, 9)) 

    prob = tf.nn.sparse_softmax_cross_entropy_with_logits(labels=A, logits=output)
    nll_loss = tf.reduce_mean(prob*L)

    similarity = tf.reduce_mean(tf.nn.sparse_softmax_cross_entropy_with_logits(labels=init, logits=output))*0.1

    sudoku = tf.cast(tf.argmax(output, axis=2)+1, tf.int32)
    print("output", output.shape)
    entropy_loss = compute_entropy_loss(output, 1.0)
    alldiff_loss = compute_alldiff_loss(output, alldiff_constraints, sample)

    total_loss = entropy_loss + alldiff_loss + nll_loss
    optimizer_loss = 3e-3*(entropy_loss) + alldiff_loss + 1e-1*nll_loss
  

    session_config = tf.ConfigProto(allow_soft_placement=True)
    session_config.gpu_options.allow_growth = True 

    sess = tf.Session(config=session_config)

    global_step = tf.Variable(initial_value=0, trainable=False)
    optimizer = tf.train.AdamOptimizer(learning_rate= 1e-3)
    train_op = optimizer.minimize(optimizer_loss, global_step = global_step)
    init_op = optimizer.minimize(similarity, global_step = global_step)


    sess.run(tf.global_variables_initializer())
    feed_dict={}
    feed_dict[A] = ans
    feed_dict[L] = label
    feed_dict[init] = initial
    feed_dict[keep_prob] = kp
    feed_dict[sample] = np.random.binomial(1, kp, size = [1,27])
    for i in range(m):
        res = sess.run([init_op, sudoku], feed_dict)
        init_config = res[-1].reshape(-1, 9, 9)
        init_loss = np.sum(np.abs(init_config - initial_solu))/81.0
        print("round-%d init_loss: %.6f"%(i, init_loss))
        if (init_loss == 0):
            break

    for i in range(n):

        res = sess.run([total_loss, entropy_loss, alldiff_loss, nll_loss, train_op, sudoku], feed_dict)
        print("iter %d total_loss = %.6f entropy_loss = %.6f alldiff_loss = %.6f nll_loss = %.6f kp = %.3f" \
            %(i, res[0], res[1], res[2], res[3], kp))

        solu = res[-1].reshape(-1, 9,9)
        puzzleAcc, n_err = PuzzleAcc(Q, solu)
        print("puzzleAcc = %.6f"%puzzleAcc)
        if ((i + 1) % n_step == 0):
            #kp = min(kp + 0.1, 0.3)
            feed_dict[sample] = np.random.binomial(1, kp, size = [1,27])
            feed_dict[keep_prob] = kp
        if (puzzleAcc == 1):
            return 1
    return 0


def get_puzzle(A, n):
    A = A[0]
    indices = []
    for i in range(9):
        for j in range(9):
            indices.append([i,j])
    np.random.shuffle(indices)
    Q = np.copy(A)
    cnt = 0
    for k in range(81):
        i = indices[k][0]
        j = indices[k][1]
        if (cnt==n):
            break
        if (Q[i][j]!=0):
            cnt +=1
            Q[i][j]=0
    return np.asarray([Q])
Q2   = np.asarray([[
    [6,9,3,7,8,4,5,1,2],
    [4,8,7,5,1,2,9,3,6],
    [1,2,5,9,6,3,8,7,4],
    [9,3,2,6,5,1,4,8,7],
    [5,6,8,2,4,7,3,9,1],
    [7,4,1,3,9,8,6,2,5],
    [3,1,9,4,7,5,2,6,8],
    [8,5,6,1,2,9,7,4,3],
    [2,7,4,8,3,6,1,5,9]
    ]], dtype="int")

Q3   = np.asarray([[ #hardest 
    [8,0,0,0,0,0,0,0,0],
    [0,0,3,6,0,0,0,0,0],
    [0,7,0,0,9,0,2,0,0],
    [0,5,0,0,0,7,0,0,0],
    [0,0,0,0,4,5,7,0,0],
    [0,0,0,1,0,0,0,3,0],
    [0,0,1,0,0,0,0,6,8],
    [0,0,8,5,0,0,0,1,0],
    [0,9,0,0,0,0,4,0,0]
    ]], dtype="int")


Q1 = np.asarray([[
[0,0,0,6,0,0,4,0,0],
[0,0,0,0,7,0,0,0,8],
[0,2,0,0,0,0,1,0,6],
[0,0,1,0,0,0,0,0,0],
[0,0,0,0,3,0,0,7,0],
[0,6,0,0,0,0,0,0,0],
[5,0,0,0,0,0,0,3,0],
[0,0,0,0,0,2,0,0,0],
[0,0,4,1,0,6,0,0,0]
]], dtype="int")

Q4 = np.asarray([[
[0,0,0,0,0,4,5,0,0],
[0,0,0,0,0,0,0,0,6],
[0,0,0,0,0,0,0,0,0],
[9,0,0,0,0,0,0,0,0],
[0,6,8,0,0,0,0,0,0],
[0,0,1,0,0,0,0,0,0],
[0,0,0,0,0,0,0,0,0],
[0,0,0,0,2,0,0,4,0],
[0,0,0,8,0,0,0,0,0]
]], dtype="int")

initial_solu =  np.asarray([[
[8,5,3,6,1,9,4,2,7],
[6,1,9,2,7,4,3,5,8],
[4,2,7,5,8,3,1,9,6],
[3,5,1,9,6,5,9,4,2],
[9,4,2,8,3,1,6,7,5],
[7,6,5,4,2,5,8,1,3],
[5,9,6,7,4,8,2,3,1],
[1,7,8,3,9,2,5,6,4],
[2,3,4,1,5,6,7,8,9]
]], dtype="int")

"""
[9,8,3,6,1,5,4,2,7],
[6,1,5,2,7,4,3,9,8],
[4,2,7,9,8,3,1,5,6],
[3,5,1,7,6,9,8,4,2],
[8,4,2,5,3,1,6,7,9],
[7,6,9,4,2,8,5,1,3],
[5,9,6,8,4,7,2,3,1],
[1,7,8,3,5,2,9,6,4],
[2,3,4,1,9,6,7,8,5],

[[4 7 6 8 3 9 5 1 2]
 [9 8 3 1 5 2 4 7 6]
 [1 2 5 4 6 7 3 9 8]
 [5 1 8 2 4 6 9 3 7]
 [6 9 2 7 1 3 8 4 5]
 [3 4 7 9 8 5 6 2 1]
 [8 6 1 3 7 4 2 5 9]
 [2 5 4 6 9 1 7 8 3]
 [7 3 9 5 2 8 1 6 4]]

"""
np.random.seed(19950420)
data = np.load("../data/test_data.npy")
Q = [data[-1][1]]
print(Q)
for i in range(10):
    solved = solve(get_puzzle(Q, 81 - 0), initial_solu, n=10000, kp = 0.5, n_step = 1)
    if (solved):
        print("solved!")
        break
