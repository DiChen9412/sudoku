import tensorflow as tf
import tensorflow.contrib.slim as slim
import numpy as np

keep_prob = 0.5
def solve_BSL(m, n):
    if (m > n):
        printf("Wrong setting!")
        return 
    bs = 1
    P = np.random.randn(m, n, n)

    P_t = tf.Variable(P, dtype=tf.float32)
    P_flat = tf.reshape(P_t, [m*n, n])


    session_config = tf.ConfigProto(allow_soft_placement=True)
    session_config.gpu_options.allow_growth = True 

    sess = tf.Session(config=session_config)

    global_step = tf.Variable(initial_value=0, trainable=False)

    sess.run(tf.global_variables_initializer())

    P_r = sess.run(P_flat)
    print(P_r)
    print(P)
    return 0

solve_BSL(2,3)


