import tensorflow as tf
import tensorflow.contrib.slim as slim
import numpy as np
def get_BLS(BLS_perm):
    m = BLS_perm.shape[0]
    n = BLS_perm.shape[1]
    BLS = np.zeros((m,n), dtype="int32")
    for i in range(m):
        for j in range(n):
            BLS[i][BLS_perm[i][j]]=j+1
    return BLS

def compute_real_dis(BLS_perm):
    m = BLS_perm.shape[0]
    n = BLS_perm.shape[1]
    BLS = get_BLS(BLS_perm)

    Errs = []
    for i in range(m):
        if (len(list(set(BLS_perm[i,:])))!=n):
            Errs.append("row :%d"%i)
            
    for i in range(n):
        if (len(list(set(BLS_perm[:,i])))!=m):
            Errs.append("col :%d"%i)

    dis = np.zeros((n,n))
    for i in range(m):
        for a in range(n):
            for b in range(n):
                x = BLS[i][a] - 1
                y = BLS[i][b] - 1
                dis[x][y] += np.abs(a-b)
    res = 0 
    for j in range(n):
        for i in range(j):
            res += np.abs(dis[i][j] - m*(n+1)/3.0)
    print(len(Errs), Errs)
    return len(Errs), res

def col_alldiff(n, m): #produce all diff sets
    idx = np.arange(n*m).reshape(m, n)
    all_diff = []
    for i in range(n):
        all_diff.append(idx[:,i])
    all_diff = np.asarray(all_diff, dtype="int32")
    return all_diff #(n, m)

def row_alldiff(n, m): #produce all diff sets
    idx = np.arange(n*m).reshape(m, n)
    all_diff = []
    for i in range(m):
        all_diff.append(idx[i,:])
    all_diff = np.asarray(all_diff, dtype="int32")
    return all_diff #(m, n)


def compute_entropy_loss(P, keep_prob):
    """
    :param P: (m, n, n)
    """
    H = tf.reduce_sum(-P * tf.log(P), axis=2)
    loss = slim.dropout(H, keep_prob = 1)
    return tf.reduce_mean(loss)

def compute_alldiff_loss(P, keep_prob, n, m):
    """
    :param P: (m, n, n)
    :param edges: 
    """ 
    col_edges = col_alldiff(n, m) #(n, m)
    row_edges = row_alldiff(n, m) #(m, n)
    P_flat = tf.reshape(P, [m*n, n]) #m*n, n

    col_all_diffs = tf.gather(P_flat, col_edges, axis=0) # n, m, n
    row_all_diffs = tf.gather(P_flat, row_edges, axis=0) # m, n, n

    col_all_diffs_T = tf.transpose(col_all_diffs, perm=[0,2,1]) 
    col_H = tf.matmul(col_all_diffs, col_all_diffs_T) # n*m*m
    col_H = tf.square(col_H - tf.eye(tf.shape(col_H)[2], dtype=tf.float64))
    #col_H = tf.square(col_H) * (1 - tf.eye(tf.shape(col_H)[2], dtype=tf.float64))

    row_all_diffs_T = tf.transpose(row_all_diffs, perm=[0,2,1]) 
    row_H = tf.matmul(row_all_diffs, row_all_diffs_T) # m*n*n
    row_H = tf.square(row_H - tf.eye(tf.shape(row_H)[2], dtype=tf.float64))

    row_all_diffs_D = tf.reduce_mean(row_all_diffs, axis=1) # m*n
    row_all_diff_H = -tf.reduce_sum(-row_all_diffs_D * tf.log(row_all_diffs_D), axis=1) #m

    #loss = tf.reduce_mean(slim.dropout(row_H, keep_prob = keep_prob)) + \
    loss = tf.reduce_mean(slim.dropout(row_all_diff_H, keep_prob = keep_prob)) + \
           tf.reduce_mean(slim.dropout(col_H, keep_prob = keep_prob))
    return loss

"""def compute_alldiff_loss(P, keep_prob, n, m):
    
    #:param P: (m, n, n)

    row_P = tf.reduce_mean(P, axis = 1) # m*n
    row_H = diff(row_P, _axis = 1) # m

    col_P = tf.reduce_mean(P, axis = 0) # n*n
    col_H = diff(col_P, _axis = 1) # n

    row_loss = slim.dropout(row_H, keep_prob = keep_prob)
    col_loss = slim.dropout(col_H, keep_prob = keep_prob)
    return -tf.reduce_mean(row_loss) #- tf.reduce_mean(col_loss)
"""

def compute_dis_loss(E, E_c, keep_prob):
    """
    :param P: (m, n, n)
    """
    dis_loss = tf.reshape( (tf.abs(E-E_c)), [-1,1])
    dis_loss = slim.dropout(dis_loss, keep_prob = keep_prob)
    return tf.reduce_sum(dis_loss)/2

def solve_BLS(m, n, n_epoch):
    if (m > n):
        printf("Wrong setting!")
        return 
    
    bs = 1
    lim = 1
    W_c = np.tile(np.arange(n), [n,1])
    W_c = np.abs(W_c - W_c.T)

    E_c = np.zeros((n,n)) + m*(n+1)/3.0
    for i in range(n):
        E_c[i][i] = 0
    E_c = tf.constant(E_c, dtype=tf.float64)
    #print(W_c)

    W = tf.constant(W_c, dtype=tf.float64)

    x = tf.Variable(np.random.randn(bs, m*n*n), name="w")
    weights_regularizer=slim.l2_regularizer(1e-4)

    x = slim.fully_connected(x, 1024, weights_regularizer=weights_regularizer)
    x = slim.fully_connected(x, 1024, weights_regularizer=weights_regularizer)
    #x = slim.fully_connected(x, 1024, weights_regularizer=weights_regularizer)
    x = slim.fully_connected(x, m*n*n, weights_regularizer=weights_regularizer, activation_fn=None)
    #x = tf.clip_by_value(x, -lim, lim)
    x = x #+ tf.cast(tf.random_uniform(tf.shape(x)), tf.float64)
    #x = W
    output = tf.reshape(x, (m, n, n))

    #P = tf.exp(output)
    #P = tf.transpose(P, perm=[2,0,1])
    #P = P / tf.reduce_sum(P, axis=0)
    #P = tf.transpose(P, perm=[1,2,0])
    P = tf.nn.softmax(output, axis=2)
    T = tf.tensordot(P, P, axes=[[0],[0]])
    T = tf.transpose(T, perm=[0,2,1,3])
    E = tf.reduce_sum(T * W, axis=[2,3]) # n,n

    entropy_loss = compute_entropy_loss(P, 1)
    alldiff_loss = compute_alldiff_loss(P, keep_prob = 0.5, n = n, m = m)
    dis_loss = compute_dis_loss(E, E_c, keep_prob = 1)

    BLS = tf.cast(tf.argmax(P, axis=2), tf.int32)

    optimizer_loss = 0.001*entropy_loss + alldiff_loss + 0.01*dis_loss/(n*(n-1)/2)

    global_min = -np.log(n) -np.log(m) 
    total_loss = entropy_loss + alldiff_loss + dis_loss #- global_min

    session_config = tf.ConfigProto(allow_soft_placement=True)
    session_config.gpu_options.allow_growth = True 

    sess = tf.Session(config=session_config)

    global_step = tf.Variable(initial_value=0, trainable=False)
    optimizer = tf.train.AdamOptimizer(learning_rate= 0.001)
    train_op = optimizer.minimize(optimizer_loss, global_step = global_step)
    #train_op1 = optimizer.minimize(entropy_loss, global_step = global_step)
    #train_op2 = optimizer.minimize(alldiff_loss, global_step = global_step)
    #grad = optimizer.compute_gradients(entropy_loss, var_list=tf.get_collection(tf.GraphKeys.TRAINABLE_VARIABLES, scope="w"))
    

    sess.run(tf.global_variables_initializer())

    best_dis = 1e10
    best_BLS = 0
    for i in range(n_epoch):
        res = sess.run([total_loss, alldiff_loss, dis_loss, P, BLS])
        print("iter %d total_loss = %.6f alldiff_loss = %.6f dis_loss = %.6f" \
            %(i, res[0], res[1], res[2]))
        _P = res[3]
        #print(_P[0])
        #print(np.mean(_P[0], axis=0))
        BLS_perm = res[-1]
        _BLS = get_BLS(BLS_perm)
        print(BLS_perm)
        print(_BLS)
        n_errs, real_dis = compute_real_dis(BLS_perm)
        print("real distance:", real_dis)
        if (n_errs ==0 and real_dis < best_dis):
            best_dis = real_dis
            best_BLS = res[-1]
        if (best_dis == 0):
            break

        sess.run([train_op])
    print(best_dis)
    print(best_BLS)
    return 

solve_BLS(8, 8, 10000)


