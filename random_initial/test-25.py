import tensorflow as tf
import tensorflow.contrib.slim as slim
import numpy as np

keep_prob = 0.5

def compute_entropy_loss(logits):
    """
    :param logits: (bs, 81, 9)
    """
    P = tf.nn.softmax(logits, axis=2) 
    H = tf.reduce_sum(-P * tf.log(P), axis=2)
    loss = slim.dropout(H, keep_prob = 1)
    return tf.reduce_mean(loss)

def compute_alldiff_loss(logits, edges):
    """
    :param logits: (bs, 81, 9)
    :param edges: (27, 9)
    """
    P = tf.nn.softmax(logits, axis=2)

    all_diffs = tf.gather(P, edges, axis=1) # bs*27*9*9
    all_diffs_P = tf.reduce_mean(all_diffs, axis=2)
    H = tf.reduce_sum(-all_diffs_P * tf.log(all_diffs_P), axis=2) #bs*27 : 0 ~ ln(9)
    #minH = tf.reduce_min(H, axis = 1)
    #maxH = tf.reduce_max(H, axis = 1)
    #loss = slim.dropout(H, keep_prob = keep_prob)
    #eps = 0.25
    #means = tf.cast(tf.exp(H - maxH)*(1 - 2*eps) + eps, tf.float32) # bs*27 : eps ~ 1 - eps, the larger the less important
    #sample = tf.where(tf.random_uniform(tf.shape(H)) > means, tf.ones_like(H), tf.zeros_like(H)) # bs*27
    #sample = tf.where(tf.equal(H, minH), tf.ones_like(H), tf.zeros_like(H))
    #loss = sample * H
    loss = slim.dropout(H, keep_prob = keep_prob)
    return -tf.reduce_mean(loss)

def sudoku_alldiff(size): #produce all diff sets
    size2 = size*size
    idx = np.arange(size2).reshape(size, size)
    all_diff = []
    for i in range(size):
        all_diff.append(idx[:,i])

    for i in range(size):
        all_diff.append(idx[i,:])

    sqr_size = int(np.sqrt(size))
    for i in range(sqr_size):
        for j in range(sqr_size):
            all_diff.append(idx[i*sqr_size:(i+1)*sqr_size, j*sqr_size:(j+1)*sqr_size].reshape(-1))
    all_diff = np.asarray(all_diff, dtype="int32")
    #print(all_diff.shape)
    return all_diff #(1620, 2)

def PuzzleAcc(Qs, indices, As, size):
    S = 0
    unsolved = []
    #print(Qs.shape)
    #print(As.shape)
    for idx in range(Qs.shape[0]):
        Q = Qs[idx].reshape((size,size))
        A = As[idx].reshape((size,size))
        n_err = 0
        err_type = []
        for i in range(size):
            for j in range(size):
                if (Q[i][j]!=0 and Q[i][j]!=A[i][j]):
                    n_err += 1
                    err_type.append("type-1: %d %d"%(i,j))

        for i in range(size):
            if (len(list(set(A[i,:])))!=size):
                n_err += 1
                err_type.append("type-2: %d"%(i))

        for i in range(size):    
            if (len(list(set(A[:,i])))!=size):
                n_err += 1 
                err_type.append("type-3: %d"%(i))

        sqr_size = int(np.sqrt(size))
        for i in range(sqr_size):
            for j in range(sqr_size):
                tmp = []
                for k in range(i*sqr_size, i*sqr_size+sqr_size):
                    for l in range(j*sqr_size, j*sqr_size+sqr_size):
                        tmp.append(A[k][l])
                if (len(list(set(tmp)))!=size):
                    n_err += 1
                    err_type.append("type-4: %d %d"%(i,j))
        print(Q[:9,:9])
        print(A[:9,:9])
        print(n_err, err_type)
        S+= int(n_err == 0)
        if (n_err !=0):
            #print(indices.shape, idx)
            unsolved.append(indices[idx])

    return S*1.0/Qs.shape[0], unsolved

def solve(Q, indices, n = 1000, bs =1, size=9):
    alldiff_constraints = sudoku_alldiff(size)
    weights_regularizer=slim.l2_regularizer(1e-4)

    size2 = size*size 
    label = np.greater(Q, 0).astype(int).reshape(bs, size2).astype(np.float32)
    #print(label)
    ans = np.maximum(Q.reshape(bs, size2) - 1, 0).astype(int)
    #print(ans)

    A = tf.placeholder(dtype=tf.int32,shape=[None, size2], name='A')
    L = tf.placeholder(dtype=tf.float64,shape=[None, size2], name='L')

    W = tf.Variable(np.random.randn(bs, size2, size), "w")
    x = tf.reshape(W, (bs, -1))
    x = slim.fully_connected(x, 1024, weights_regularizer=weights_regularizer)
    x = slim.fully_connected(x, 1024, weights_regularizer=weights_regularizer)
    x = slim.fully_connected(x, 1024, weights_regularizer=weights_regularizer)
    x = slim.fully_connected(x, size2*size, weights_regularizer=weights_regularizer, activation_fn=None)
    #x = W
    output = tf.reshape(x, (bs, size2, size)) 

    prob = tf.nn.sparse_softmax_cross_entropy_with_logits(labels=A, logits=output)
    nll_loss = tf.reduce_mean(prob*L)

    sudoku = tf.cast(tf.argmax(output, axis=2)+1, tf.int32)
    print("output", output.shape)
    entropy_loss = compute_entropy_loss(output)
    alldiff_loss = compute_alldiff_loss(output, alldiff_constraints)

    total_loss = entropy_loss + alldiff_loss + nll_loss
    optimizer_loss = 0.003*entropy_loss + alldiff_loss + 0.1*nll_loss
  

    session_config = tf.ConfigProto(allow_soft_placement=True)
    session_config.gpu_options.allow_growth = True 

    sess = tf.Session(config=session_config)

    global_step = tf.Variable(initial_value=0, trainable=False)
    optimizer = tf.train.AdamOptimizer(learning_rate= 1e-3)
    train_op = optimizer.minimize(optimizer_loss, global_step = global_step)

    sess.run(tf.global_variables_initializer())
    feed_dict={}
    feed_dict[A] = ans
    feed_dict[L] = label

    best_puzzleAcc = 0
    for i in range(n):
        res = sess.run([total_loss, entropy_loss, alldiff_loss, nll_loss, train_op, sudoku], feed_dict)
        print("iter %d total_loss = %.6f entropy_loss = %.6f alldiff_loss = %.6f nll_loss = %.6f" \
            %(i, res[0], res[1], res[2], res[3]))

        solu = res[-1].reshape(-1, size, size)
        puzzleAcc, unsolved = PuzzleAcc(Q, indices, solu, size)
        print("puzzleAcc = %.6f"%puzzleAcc)
        best_puzzleAcc = puzzleAcc
        if (puzzleAcc == 1):
            break
            return 1
    return best_puzzleAcc, unsolved


def get_puzzle(A, n, size):
    A = A[0]
    indices = []
    for i in range(size):
        for j in range(size):
            indices.append([i,j])
    np.random.shuffle(indices)
    Q = np.copy(A)
    cnt = 0
    for k in range(size*size):
        i = indices[k][0]
        j = indices[k][1]
        if (cnt==n):
            break
        if (Q[i][j]!=0):
            cnt +=1
            Q[i][j]=0
    return np.asarray([Q])

def get_data(data, indices):
    res = []
    for i in indices:
        res.append(data[i])
    return np.asarray(res)

data = np.load("test_data25.npy")
#data = np.zeros((1,16,16))
data = get_puzzle(data, int(25*25*0.5), size=25)
bs = 1
overall_acc = 0
n_batch = 3

indices = np.arange(data.shape[0])
#np.random.shuffle(indices)

indices = indices
Ns = [100000]
#[100, 300, 1000, 3000, 10000, 30000]
_indices = indices

for (z, _n) in enumerate(Ns): 
    test_data = get_data(data, _indices)
    next_indices = []
    n_batch = int((test_data.shape[0] - 1) / bs) + 1
    print("This is round %d with n_batch = %d"%(z, n_batch))
    for i in range(n_batch):

        Qs = test_data[i*bs:min((i+1)*bs, test_data.shape[0])]
        #print(Qs.shape)
        acc, unsolved = solve(Qs, _indices[i*bs:min((i+1)*bs, test_data.shape[0])], n = _n, bs = Qs.shape[0], size=25)

        print("epoch %d bs = %d acc=%.6f"%(i, Qs.shape[0], acc))
        overall_acc += acc * bs
        next_indices += unsolved

    _indices = np.asarray(next_indices)
    print("%d puzzles are still unsolved!"%_indices.shape[0])
    if (_indices.shape[0] == 0):
        break

print("overall acc = %.6f" % (overall_acc/indices.shape[0]))

