import tensorflow as tf
import tensorflow.contrib.slim as slim
import numpy as np

keep_prob = 0.5
def solve_BSL(m, n):
    if (m > n):
        printf("Wrong setting!")
        return 

    c = 5
    W = np.zeros((n)) #+ np.random.randn(n)
    #W[0] = 0
    #W[1] = 0
    W = tf.Variable(W, dtype=tf.float32, name="w")
    #W = tf.exp(W)
    P = tf.nn.softmax(W, axis=0)
    #P = W/tf.sqrt(tf.reduce_sum(W*W, axis=0))
    #H = tf.reduce_sum(-P * tf.log(P), axis=0)
    #H = tf.reduce_mean(tf.square(tf.matmul(P, tf.transpose(P)) - tf.eye(n))) #

    H = tf.reduce_mean(tf.reduce_sum(-P * tf.log(P), axis=0)) #

    loss = H #+ tf.random_uniform([1])*H2 

    session_config = tf.ConfigProto(allow_soft_placement=True)
    session_config.gpu_options.allow_growth = True 

    sess = tf.Session(config=session_config)

    global_step = tf.Variable(initial_value=0, trainable=False)
    optimizer = tf.train.AdamOptimizer(learning_rate= 0.1, beta1=0.9, beta2=0.999, epsilon=1e-08)
    train_op = optimizer.minimize(loss)
    perturb = W.assign_add(tf.random_normal(W.shape)*0.01)

    grad = optimizer.compute_gradients(loss, var_list=tf.get_collection(tf.GraphKeys.TRAINABLE_VARIABLES, scope="w"))
    #grad2 = -H*P - tf.log(P)*P
    #grad2 = -2.0*P*(P - tf.reduce_sum(tf.square(P), axis=0))

    sess.run(tf.global_variables_initializer())
    
    #print(tf.get_collection(tf.GraphKeys.TRAINABLE_VARIABLES, scope="w"))
    for i in range(100):
        _, _H, grad_w, _P, _W = sess.run([train_op, H, grad, P, W])
        
        print(grad_w)
        print(_P)
        print(_H) 

        sess.run(perturb)
    return 0   

solve_BSL(1,5)


