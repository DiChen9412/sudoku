import numpy as np

# The original 49,151 minimum sudoku's with 17 givens are from Gordon Royle (http://staffhome.ecm.uwa.edu.au/~00013890/sudokumin.php)
# 
# I've found all the solutions using Norvigs code (http://norvig.com/sudoku.html) and uploaded the file with puzzles and solutions in one file

def extract(s):
    res = np.zeros((9,9), dtype="int")
    for i in range(9):
        for j in range(9):
            res[i][j]=int(s[i*9+j])
    return res

def display(s):
    for i in range(9):
        for j in range(9):
            print s[i][j],
        print " "
    print " "

def get_mapping():
    num_perm = np.arange(10)
    np.random.shuffle(num_perm[1:])

    T = np.random.randint(2)

    col_perm = np.arange(9)
    #print col_perm 
    np.random.shuffle(col_perm[:3])
    np.random.shuffle(col_perm[3:6])
    np.random.shuffle(col_perm[6:9])

    row_perm = np.arange(9)
    np.random.shuffle(row_perm[:3])
    np.random.shuffle(row_perm[3:6])
    np.random.shuffle(row_perm[6:9])

    #print col_perm 
    col_perm = col_perm.reshape((3,3))
    row_perm = row_perm.reshape((3,3))

    #print col_perm
    np.random.shuffle(col_perm)
    np.random.shuffle(row_perm)

    #print col_perm
    col_perm = col_perm.reshape(-1)
    row_perm = row_perm.reshape(-1)
    
    #print col_perm


    return num_perm, T, col_perm, row_perm

def perm(s, mapping):
    num_perm = mapping[0]
    T = mapping[1]
    col_perm = mapping[2]
    row_perm = mapping[3]

    res = np.zeros((9,9), dtype="int")
    for i in range(9):
        for j in range(9):
            res[i][j] = num_perm[s[row_perm[i]][col_perm[j]]]
    if (T == 1):
        res = res.T
    return res

"""
url = "https://www.dropbox.com/s/bcf5q30oryg0csw/sudoku17.txt?dl=1"
fname = "/tmp/sudoku17.txt"

f = open("sudoku17.txt", "r")

data = [line.strip().split(',') for line in f]

print len(data)

sudoku =[]
for (ques,ans) in data:
    ques = extract(ques)
    ans = extract(ans)
    #display(ques)
    #display(ans)
    sudoku.append((ques, ans))

np.save("minimum",sudoku)
"""

sudoku = np.load("minimum.npy")

perm_sudoku = []
for (ques,ans) in sudoku:
    for i in range(5):
        mapping = get_mapping()
        #print mapping
        Q = perm(ques, mapping)
        A = perm(ans, mapping)
        #display(Q)
        #display(A)
        #break
        perm_sudoku.append((Q, A))
    #break
    
print len(perm_sudoku)
np.save("perm_sudoku", perm_sudoku)

