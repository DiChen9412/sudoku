from model import MODEL
import time 
import config
import tensorflow as tf
import numpy as np
FLAGS = tf.app.flags.FLAGS

def PuzzleAcc(Qs, As):
    S = 0
    #print(Qs.shape)
    #print(As.shape)
    for k in range(Qs.shape[0]):
        Q = Qs[k].reshape((9,9))
        A = As[k].reshape((9,9))
        s = 1
        for i in range(9):
            for j in range(9):
                if (Q[i][j]!=0 and Q[i][j]!=A[i][j]):
                    s = 0
        #print(s)
        for i in range(9):
            if (len(list(set(A[i,:])))!=9 or len(list(set(A[:,i])))!=9):
                s = 0 
        #print(s)
        for i in range(3):
            for j in range(3):
                tmp = []
                for k in range(i*3, i*3+3):
                    for l in range(j*3, j*3+3):
                        tmp.append(A[k][l])
                if (len(list(set(tmp)))!=9):
                    s = 0
        #print(s)
        print(Q)
        print(A)
        print(s)
        S+=s
    return S*1.0/Qs.shape[0]

def test(model):
    model.load(FLAGS.checkpoint_path)
    test_data = np.load(FLAGS.test_data)
    test_n_batch = int(test_data.shape[0]/(FLAGS.batch_size*5))
    print("test_n_batch", test_n_batch)
    test_loss = 0
    test_digit_acc = 0
    test_puzzle_acc = 0
    test_puzzle_acc2 = 0

    for i in range(test_n_batch):
        _loss, _quizzes, _predicted, _logits, _answers, _digit_acc, _puzzle_acc = model.test_batch()
        test_loss += _loss
        test_digit_acc += _digit_acc
        test_puzzle_acc2 += _puzzle_acc
        test_puzzle_acc += PuzzleAcc(_quizzes, _predicted[-1])#_puzzle_acc
        print(f"batch:{i}")

    test_loss /= test_n_batch
    test_digit_acc /= test_n_batch 
    test_puzzle_acc /= test_n_batch
    test_puzzle_acc2 /=test_n_batch
    print(f"test: total_loss={test_loss}, digit_acc={test_digit_acc: .5f}, puzzle_acc={test_puzzle_acc: .5f}, puzzle_acc2={test_puzzle_acc2: .5f}") 
    
test(MODEL())
