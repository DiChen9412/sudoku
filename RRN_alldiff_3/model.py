import numpy as np
import tensorflow as tf
import tensorflow.contrib.slim as slim
from tensorflow.contrib import layers
from tensorflow.python.client import device_lib
import os 
import sys
if (os.path.dirname(sys.argv[0])!=''):
    os.chdir(os.path.dirname(sys.argv[0]))

import get_data 
FLAGS = tf.app.flags.FLAGS


def CNN(input_image, is_training):

    weights_regularizer=slim.l2_regularizer(FLAGS.weight_decay)
    batch_norm = slim.batch_norm
    batch_norm_params = {'is_training':is_training,'updates_collections':tf.GraphKeys.UPDATE_OPS,'decay':0.9,'epsilon':0.00001}

    x = input_image # bs, 81, 16*3， 1
    #print("layershape",x)
    x = slim.conv2d(scope='encoder/image/conv1',inputs=x,num_outputs=32,kernel_size=[3,3],stride=[1,1],
        normalizer_fn=slim.batch_norm,normalizer_params=batch_norm_params,weights_regularizer = weights_regularizer)

    x = slim.max_pool2d(scope='encoder/image/pool1',inputs=x,kernel_size=[2,2],stride=[2,2],padding='SAME')

    x = slim.conv2d(scope='encoder/image/conv2',inputs=x,num_outputs=64,kernel_size=[3,3],stride=[1,1],
        normalizer_fn=slim.batch_norm,normalizer_params=batch_norm_params,weights_regularizer = weights_regularizer)        

    x = slim.max_pool2d(scope='encoder/image/pool2',inputs=x,kernel_size=[2,2],stride=[2,2],padding='SAME')

    x = slim.conv2d(scope='encoder/image/conv3',inputs=x,num_outputs=81,kernel_size=[3,3],stride=[1,1],
        normalizer_fn=slim.batch_norm,normalizer_params=batch_norm_params,weights_regularizer = weights_regularizer)        

    #x = slim.max_pool2d(scope='encoder/image/pool3',inputs=x,kernel_size=[2,2],stride=[2,2],padding='SAME')
    print("layer_shape",x)

    x = tf.reshape(x,[-1,21*12*81])

    print("output_shape",x)
    #x = slim.fully_connected(x, 81*FLAGS.n_hidden, weights_regularizer=weights_regularizer)
    return x


def print_vars(vars):
    total = 0
    for var in vars:
        print(var.name, var.get_shape())
        total += np.prod(var.get_shape().as_list())
    print("#parameters =", total)

def get_devices():
    gpus = [x.name for x in (device_lib.list_local_devices()) if x.device_type == 'GPU']
    if len(gpus) > 0:
        devices = gpus
    else:
        print("WARNING: No GPU's found. Using CPU")
        devices = ['cpu:0']

    print("Using devices: ", devices)
    return devices

def avg_n(x):
    return tf.reduce_mean(tf.stack(x, axis=0), axis=0)

def average_gradients(tower_grads, name='avg-grads'):
    """Calculate the average gradient for each shared variable across all towers.
    Note that this function provides a synchronization point across all towers.
    Args:
      tower_grads: List of lists of (gradient, variable) tuples. The outer list
        is over individual gradients. The inner list is over the gradient
        calculation for each tower.
    Returns:
       List of pairs of (gradient, variable) where the gradient has been averaged
       across all towers.
    """
    with tf.name_scope(name):
        average_grads = []
        for grad_and_vars in zip(*tower_grads):
            # Note that each grad_and_vars looks like the following:
            #   ((grad0_gpu0, var0_gpu0), ... , (grad0_gpuN, var0_gpuN))

            # Keep in mind that the Variables are redundant because they are shared
            # across towers. So .. we will just return the first tower's pointer to
            # the Variable.
            v = grad_and_vars[0][1]

            grads = []
            for g, _ in grad_and_vars:
                grads.append(g)
            grad = avg_n(grads)

            """
            grads = []
            for g, _ in grad_and_vars:
                # Add 0 dimension to the gradients to represent the tower.
                expanded_g = tf.expand_dims(g, 0)

                # Append on a 'tower' dimension which we will average over below.
                grads.append(expanded_g)

            # Average over the 'tower' dimension.
            grad = tf.concat(axis=0, values=grads)
            grad = tf.reduce_mean(grad, 0)
            """
            grad_and_var = (grad, v)
            average_grads.append(grad_and_var)
        return average_grads
def compute_entropy_loss(logits):
    """
    :param logits: (bs, 81, 9)
    """
    P = tf.nn.softmax(logits, axis=2) 
    H = tf.reduce_sum(-P * tf.log(P), axis=2)
    return tf.reduce_mean(H)

def compute_alldiff_loss(logits, edges):
    """
    :param logits: (bs, 81, 9)
    :param edges: (27, 9)
    """
    P = tf.nn.softmax(logits, axis=2)

    all_diffs = tf.gather(P, edges, axis=1) # bs*27*9*9
    all_diffs_P = tf.reduce_mean(all_diffs, axis=2)
    H = tf.reduce_sum(-all_diffs_P * tf.log(all_diffs_P), axis=2) #bs*27
    return -tf.reduce_mean(H)

def message_passing(nodes, edges, sink_indices, edge_features, message_fn, edge_keep_prob=1.0):
    """
    n_nodes = bs*81

    Pass messages between nodes and sum the incoming messages at each node.
    Implements equation 1 and 2 in the paper, i.e. m_{.j}^t &= \sum_{i \in N(j)} f(h_i^{t-1}, h_j^{t-1})
    :param nodes: (n_nodes, n_features) tensor of node hidden states.
    :param edges: (n_edges, 2) tensor of indices (i, j) indicating an edge from nodes[i] to nodes[j].
    :param sink_indices: (n_edges, 1) the sink of all edges
    :param edge_features: features for each edge. Set to zero if the edges don't have features.
    :param message_fn: message function, will be called with input of shape (n_edges, 2*n_features + edge_features). The output shape is (n_edges, n_outputs), where you decide the size of n_outputs
    :param edge_keep_prob: The probability by which edges are kept. Basically dropout for edges. Not used in the paper.
    :return: (n_nodes, n_output) Sum of messages arriving at each node.
    """
    n_nodes = tf.shape(nodes)[0]
    n_features = nodes.get_shape()[1]
    n_edges = tf.shape(edges)[0]

    #print("nodes", nodes)
    #print("edges", edges)
    message_inputs = tf.gather(nodes, edges)  # n_edges, 2, n_features

    reshaped = tf.concat([tf.reshape(message_inputs, (-1, 2 * n_features)), edge_features], 1)
    

    messages = message_fn(reshaped)  # n_edges, n_output (96)
    messages = tf.nn.dropout(messages, edge_keep_prob, noise_shape=(n_edges, 1)) # n_edges, n_output (96)

    n_output = messages.get_shape()[1]


    #idx_i, idx_j = tf.split(edges, 2, 1) #(n_edges,1),  (n_edges,1)
    out_shape = (n_nodes, n_output)
    updates = tf.scatter_nd(sink_indices, messages, out_shape)

    return updates

class MODEL:
    batch_size = FLAGS.batch_size
    emb_size = FLAGS.emb_size 
    n_hidden = FLAGS.n_hidden
    n_infer_step = FLAGS.n_infer_step
    devices = get_devices()
    def __init__(self):
        #tf.set_random_seed(19950420)

        with tf.Graph().as_default(), tf.device("/cpu:0"):
            self.train, self.valid, self.test = get_data.get_data()
            self.mode = tf.placeholder(tf.string)
            self.regularizer = layers.l2_regularizer(FLAGS.weight_decay)

            session_config = tf.ConfigProto(allow_soft_placement=True)
            session_config.gpu_options.allow_growth = True

            self.sess = tf.Session(config=session_config)
            
            self.global_step = tf.Variable(initial_value=0, trainable=False)
            self.optimizer = tf.train.AdamOptimizer(learning_rate= FLAGS.learning_rate)


            edges = self.sudoku_edges()
            edge_indices = tf.constant([(i + (b * 81), j + (b * 81)) for b in range(self.batch_size) for i, j in edges], tf.int32)
            _, sink_indices = tf.split(edge_indices, 2, 1) 

            alldiff_constraints = self.sudoku_alldiff()
            #alldiff_indices = tf.constant([[ idx+(b*81) for idx in diff] for b in range(self.batch_size) for diff in alldiff_constraints], tf.int32)
            
            # bs*1620
            n_edges = tf.shape(edge_indices)[0]
            edge_features = tf.zeros((n_edges, 1), tf.float32)

            #create embeddings for rows, columns 
            positions = tf.constant([ [(i, j) for i in range(9) for j in range(9)] for b in range(self.batch_size) ], tf.int32)  # (bs, 81, 2)
            rows = layers.embed_sequence(positions[:, :, 0], 9, self.emb_size, scope='rows-embeddings', unique=True)  # bs, 81, emb_size
            cols = layers.embed_sequence(positions[:, :, 1], 9, self.emb_size, scope='cols-embeddings', unique=True)  # bs, 81, emb_size

            towers = []
            with tf.variable_scope(tf.get_variable_scope()):
                for device_idx, device in enumerate(self.devices):
                    with tf.device('/cpu:0'):
                        #get the batch data 
                        (Q, A) = tf.cond(tf.equal(self.mode, "train"),
                             true_fn=lambda: self.train.get_next(),
                             false_fn=lambda: tf.cond(tf.equal(self.mode, "valid"), 
                             true_fn=lambda: self.valid.get_next(), false_fn=lambda: self.test.get_next()
                        ))

                        x = layers.embed_sequence(Q, 10, self.emb_size, scope='digits-embeddings', unique=True)  # bs, 81, emb_size
                        x = tf.concat([x, rows, cols], axis=2) # bs * 81 * (3*emb_size)
                        #x = tf.reshape(x, (-1, 3 * self.emb_size)) #(bs * 81) * (3*emb_size)

                    with tf.device(device), tf.name_scope("device-%s" % device_idx): #with tf.device(FLAGS.model_device):
                        #x = self.mlp(x, 'pre-mlp') #(bs * 81), n_hidden
                        x0 = x #original info 

                        n_nodes = tf.shape(x)[0]
                        outputs = []
                        log_losses = []
                        entropy_losses = []
                        alldiff_losses = []
                        with tf.variable_scope("steps"):
                            x = tf.expand_dims(x, -1) # bs, 81, 3*emb_size, 1
                            x = CNN(x, tf.equal(self.mode, "train")) # bs, 81*y
                            print(x.get_shape()[1])
                            CNN_size = 252
                            x = tf.reshape(x, (-1, CNN_size))

                            #x = tf.reshape(x, ())
                            x = self.mlp(x, "post-mlp")
                            with tf.variable_scope('graph-sum'):
                                out = tf.reshape(layers.fully_connected(x, num_outputs=9, activation_fn=None), (-1, 81, 9)) #output logits for each step   bs, 81, 9
                                entropy_losses.append(compute_entropy_loss(out))         
                                alldiff_losses.append(compute_alldiff_loss(out, alldiff_constraints))
                                outputs.append(out)
                                log_losses.append(tf.reduce_mean(tf.nn.sparse_softmax_cross_entropy_with_logits(labels=(A-1), logits=out)))
                                #n_infer_step, 1
                            tf.get_variable_scope().reuse_variables() #set reuse for lstm, message-mlp

                        self.l2_loss = tf.add_n(tf.get_collection(tf.GraphKeys.REGULARIZATION_LOSSES))
                        total_loss = 0*avg_n(log_losses) + avg_n(entropy_losses) + avg_n(alldiff_losses) + 0*self.l2_loss #avg_loss over steps
                        
                        towers.append({
                            'total_loss': total_loss,
                            'grads': self.optimizer.compute_gradients(total_loss),
                            'grads2': self.optimizer.compute_gradients(avg_n(entropy_losses)),
                            'grads3': self.optimizer.compute_gradients(avg_n(alldiff_losses)),
                            'log_losses': tf.stack(log_losses),  # (n_steps, 1)
                            'entropy_losses': tf.stack(entropy_losses),
                            'alldiff_losses': tf.stack(alldiff_losses),
                            'Q': Q,  # (bs, 81, 10)
                            'A': A,  # (bs, 81, 10)
                            'outputs': tf.stack(outputs)  # n_steps, bs, 81, 10
                        })

                        tf.get_variable_scope().reuse_variables() #set reuse across towers

            self.total_loss = avg_n([t['total_loss'] for t in towers])
            self.outputs = tf.concat([t['outputs'] for t in towers], axis=1) # n_step, bs*n_GPU, 81, 10
            self.predicted = tf.cast(tf.argmax(self.outputs, axis=3)+1, tf.int32)  # value: 1~9
            self.Q = tf.concat([t['Q'] for t in towers], axis=0) #bs*n_GPU, 81
            self.A = tf.concat([t['A'] for t in towers], axis=0) #bs*n_GPU, 81

            log_losses = avg_n([t['log_losses'] for t in towers])
            entropy_losses = avg_n([t['entropy_losses'] for t in towers])
            alldiff_losses = avg_n([t['alldiff_losses'] for t in towers])

            tf.summary.scalar('losses/total', self.total_loss)
            tf.summary.scalar('losses/l2', self.l2_loss)

            for step in range(self.n_infer_step):
                equal = tf.equal(self.A, self.predicted[step])  #bs*n_GPU, 81 bool 

                digit_acc = tf.reduce_mean(tf.to_float(equal))
                tf.summary.scalar('steps/%d/digit-acc' % step, digit_acc)

                puzzle_acc = tf.reduce_mean(tf.to_float(tf.reduce_all(equal, axis=1))) # logitcal and 
                tf.summary.scalar('steps/%d/puzzle-acc' % step, puzzle_acc)

                tf.summary.scalar('steps/%d/losses/log' % step, log_losses[step])
                tf.summary.scalar('steps/%d/losses/entropy' % step, entropy_losses[step])
                tf.summary.scalar('steps/%d/losses/alldiff' % step, alldiff_losses[step])
                
                self.digit_acc = digit_acc
                self.puzzle_acc = puzzle_acc
                self.alldff_loss = alldiff_losses[step]
                self.entropy_loss = entropy_losses[step]

            avg_grads = average_gradients([t['grads'] for t in towers]) #avg grads over towers
            avg_grads2 = average_gradients([t['grads2'] for t in towers]) #avg grads2 over towers
            avg_grads3 = average_gradients([t['grads3'] for t in towers]) #avg grads3 over towers

            self.train_step = self.optimizer.apply_gradients(avg_grads, global_step = self.global_step)
            self.train_step2 = self.optimizer.apply_gradients(avg_grads2, global_step = self.global_step)
            self.train_step3 = self.optimizer.apply_gradients(avg_grads3, global_step = self.global_step)
            

            self.sess.run(tf.global_variables_initializer())
            self.saver = tf.train.Saver(max_to_keep=FLAGS.max_keep)
            print_vars(tf.trainable_variables())

            self.train_writer = tf.summary.FileWriter(FLAGS.train_summary_dir, self.sess.graph)
            self.valid_writer = tf.summary.FileWriter(FLAGS.valid_summary_dir, self.sess.graph)
            self.test_writer = tf.summary.FileWriter(FLAGS.test_summary_dir, self.sess.graph)
            self.summaries = tf.summary.merge_all()

    def save(self, current_step):
        #print('saving model')
        saved_model_path = self.saver.save(self.sess, FLAGS.model_dir+'model', global_step=current_step)
        #print("rewriting the number of model to config.py")
        configFile=open( FLAGS.config_dir, "r")
        content=[line.strip("\n") for line in configFile]
        configFile.close()

        for i in range(len(content)):
            if ("checkpoint_path" in content[i]):
                content[i]="tf.app.flags.DEFINE_string('checkpoint_path', './model/model-%d','The path to a checkpoint from which to fine-tune.')"%current_step
        
        configFile=open( FLAGS.config_dir, "w")
        for line in content:
            configFile.write(line+"\n")
        configFile.close()
        print('have saved model to ', saved_model_path)

    def load(self, path):
        print("Loading %s..." % path)
        self.saver.restore(self.sess, path)

    def train_batch(self):
        _, _loss, _logits, _summaries, _step, _digit_acc, _puzzle_acc, _alldff_loss, _entropy_loss = self.sess.run([self.train_step, self.total_loss, self.outputs, self.summaries, self.global_step, self.digit_acc, self.puzzle_acc, self.alldff_loss, self.entropy_loss], {self.mode: 'train'})
        
        if _step % FLAGS.check_freq == 0:
            self.train_writer.add_summary(_summaries, _step)
            print(f"training: step={_step}, total_loss={_loss}, digit_acc={_digit_acc: .2f}, puzzle_acc={_puzzle_acc: .2f}, alldiff={_alldff_loss: .6f},  entropy={_entropy_loss: .6f}")

        return _loss
    
    def train_batch2(self):
        _, _loss, _logits, _summaries, _step, _digit_acc, _puzzle_acc, _alldff_loss, _entropy_loss = self.sess.run([self.train_step2, self.total_loss, self.outputs, self.summaries, self.global_step, self.digit_acc, self.puzzle_acc, self.alldff_loss, self.entropy_loss], {self.mode: 'train'})
        
        if _step % FLAGS.check_freq == 0:
            self.train_writer.add_summary(_summaries, _step)
            print(f"training: step={_step}, total_loss={_loss}, digit_acc={_digit_acc: .2f}, puzzle_acc={_puzzle_acc: .2f}, alldiff={_alldff_loss: .6f},  entropy={_entropy_loss: .6f}")

        return _loss, _entropy_loss, _alldff_loss
    
    def train_batch3(self):
        _, _loss, _logits, _summaries, _step, _digit_acc, _puzzle_acc, _alldff_loss, _entropy_loss = self.sess.run([self.train_step3, self.total_loss, self.outputs, self.summaries, self.global_step, self.digit_acc, self.puzzle_acc, self.alldff_loss, self.entropy_loss], {self.mode: 'train'})
        
        if _step % FLAGS.check_freq == 0:
            self.train_writer.add_summary(_summaries, _step)
            print(f"training: step={_step}, total_loss={_loss}, digit_acc={_digit_acc: .2f}, puzzle_acc={_puzzle_acc: .2f}, alldiff={_alldff_loss: .6f},  entropy={_entropy_loss: .6f}")

        return _loss, _entropy_loss, _alldff_loss

    def val_batch(self):
        _loss, _predicted, _answers, _summaries, _step, _digit_acc, _puzzle_acc, _alldff_loss, _entropy_loss = self.sess.run([self.total_loss, self.predicted, self.A, self.summaries, self.global_step, self.digit_acc, self.puzzle_acc, self.alldff_loss, self.entropy_loss], {self.mode: 'valid'})
        self.valid_writer.add_summary(_summaries, _step)

        print(f"validation: step={_step}, total_loss={_loss}, digit_acc={_digit_acc: .2f}, puzzle_acc={_puzzle_acc: .2f}, alldiff={_alldff_loss: .6f},  entropy={_entropy_loss: .6f}")
        return _loss, _digit_acc, _puzzle_acc

    def test_batch(self):
        _loss, _quizzes, _predicted, _logits, _answers, _digit_acc, _puzzle_acc = self.sess.run([self.total_loss, self.Q, self.predicted, self.outputs, self.A, self.digit_acc, self.puzzle_acc], {self.mode: 'test'})
        return _loss, _quizzes, _predicted, _logits, _answers, _digit_acc, _puzzle_acc


    def mlp(self, x, scope):
        #print(scope)
        with tf.variable_scope(scope):
            for i in range(3):
                x = layers.fully_connected(x, self.n_hidden, weights_regularizer=self.regularizer)
            return layers.fully_connected(x, self.n_hidden, weights_regularizer=self.regularizer, activation_fn=None)
    
    def sudoku_edges(self): #produce pairs of nodes which have an edge 0-80: 72*9 + 72*9 + 36*9 = 1620
        def cross(a):
            return [(i, j) for i in a.flatten() for j in a.flatten() if not i == j]

        idx = np.arange(81).reshape(9, 9)
        rows, columns, squares = [], [], []
        for i in range(9):
            rows += cross(idx[i, :])
            columns += cross(idx[:, i])
        for i in range(3):
            for j in range(3):
                squares += cross(idx[i * 3:(i + 1) * 3, j * 3:(j + 1) * 3])
        return list(set(rows + columns + squares)) #(1620, 2)

    def sudoku_alldiff(self): #produce all diff sets
        idx = np.arange(81).reshape(9, 9)
        all_diff = []
        for i in range(9):
            all_diff.append(idx[:,i])

        for i in range(9):
            all_diff.append(idx[i,:])

        for i in range(3):
            for j in range(3):
                all_diff.append(idx[i*3:(i+1)*3, j*3:(j+1)*3].reshape(-1))
        all_diff = np.asarray(all_diff, dtype="int32")
        #print(all_diff.shape)
        return all_diff #(1620, 2)
