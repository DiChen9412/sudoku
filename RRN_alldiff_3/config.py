import tensorflow as tf
FLAGS = tf.flags.FLAGS
tf.flags.DEFINE_string('model_dir', './model/','path to store model')
tf.flags.DEFINE_string('train_summary_dir', './summary/train','path to store summary_dir for training')
tf.flags.DEFINE_string('valid_summary_dir', './summary/valid','path to store summary_dir for validation')
tf.flags.DEFINE_string('test_summary_dir', './summary/test','path to store summary_dir for test')
tf.flags.DEFINE_string('config_dir', './config.py','path to config.py')
tf.app.flags.DEFINE_string('checkpoint_path', './model/model-1499','The path to a checkpoint from which to fine-tune.')
tf.flags.DEFINE_string('visual_dir', 'visualization/','path to store visualization codes and data')

tf.flags.DEFINE_string('train_data', '../data/test_data00.npy','The path of training data')
tf.flags.DEFINE_string('valid_data', '../data/test_data00.npy','The path of validation data')
tf.flags.DEFINE_string('test_data', '../data/test_data00.npy','The path of testdata')
tf.flags.DEFINE_string('model_device', '/device:GPU:0','The device for training')

tf.flags.DEFINE_integer('batch_size', 1, 'number of data points in one batch') #128
tf.flags.DEFINE_float('learning_rate', 0.001, 'initial learning rate')
tf.flags.DEFINE_float('pred_lr', 1, 'the learning rate for predictor')
tf.flags.DEFINE_integer('max_epoch', 400, 'max epoch to train')
tf.flags.DEFINE_integer('pretrain_epoch', 1000, '#epoch to pre-train')
tf.flags.DEFINE_float('weight_decay', 0.0001, 'weight_decay')
tf.flags.DEFINE_float('threshold', 0, 'The threshold for prediction')
tf.flags.DEFINE_float('lr_decay_ratio', 1, 'The decay ratio of learning rate')
tf.flags.DEFINE_float('lr_decay_times', 1, 'How many times does learning rate decay')
tf.flags.DEFINE_integer('n_test_sample', 10000, 'The sampling times for testing')
tf.flags.DEFINE_integer('n_train_sample', 100, 'The sampling times for training') #100
tf.flags.DEFINE_integer('n_infer_step', 1, '# the steps for LSTM ') #100

tf.flags.DEFINE_integer('emb_size', 16, 'The embedding size for positions, digits, etc') #100
tf.flags.DEFINE_integer('n_hidden', 128, 'The hidden layer size for MLP and LSTM') #100

tf.flags.DEFINE_float('save_epoch', 2.0, 'epochs to save model')
tf.flags.DEFINE_integer('max_keep', 3, 'maximum number of saved model')
tf.flags.DEFINE_integer('check_freq', 10, 'checking frequency')



