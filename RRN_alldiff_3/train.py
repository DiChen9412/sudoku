from model import MODEL
import time 
import config
import tensorflow as tf
FLAGS = tf.app.flags.FLAGS
import numpy as np

def train(model):
    n_updates = 300000

    val_interval = 1500
    val_n_batch = 1

    start = time.time()
    best = float("inf")
    entropy_loss = 0
    alldiff_loss = 0
    for i in range(n_updates):
        """if (i == 0):#(entropy_loss >= alldiff_loss + np.log(9)):
            loss, entropy_loss, alldiff_loss = model.train_batch2() #entropy
            #print("entropy")
        else:
            loss, entropy_loss, alldiff_loss = model.train_batch3() #all_diff
            #print("all_diff")
		"""

        #print(entropy_loss, alldiff_loss + np.log(9))
        loss = model.train_batch()
        if (i+1) % val_interval == 0:
            took = time.time() - start
            print("%05d/%05d %f updates/s %f loss" % (i, n_updates, val_interval / took, loss))

            val_loss = 0
            val_digit_acc = 0
            val_puzzle_acc =0
            for j in range(val_n_batch):
                #print(j)
                iter_loss, iter_digit_acc, iter_puzzle_acc = model.val_batch()
                val_loss += iter_loss
                val_digit_acc += iter_digit_acc
                val_puzzle_acc += iter_puzzle_acc

            val_loss /= val_n_batch
            val_digit_acc /= val_n_batch 
            val_puzzle_acc /= val_n_batch
            print(f"validation: total_loss={val_loss}, digit_acc={val_digit_acc: .2f}, puzzle_acc={val_puzzle_acc: .2f}")
            if (val_loss < best):
                best = val_loss
                model.save(i)
            start = time.time()
    model.save(n_updates)

train(MODEL())
