import tensorflow as tf
import tensorflow.contrib.slim as slim
import numpy as np
import datetime
import config 
from tensorflow.contrib import layers

FLAGS = tf.app.flags.FLAGS
def encode(data, n_repeat):
	q, a = zip(*(list(data)))
	q, a = np.asarray(q, np.int32),  np.asarray(a, np.int32)
	q = q.reshape((q.shape[0],81))
	a = a.reshape((a.shape[0],81))
	print(data.shape)
	#print(a.shape)
	return tf.data.Dataset.from_tensor_slices((q, a)).shuffle(FLAGS.batch_size * 1000)\
    .repeat(n_repeat).batch(FLAGS.batch_size).make_one_shot_iterator()

def get_data():
	train_data = np.load(FLAGS.train_data)
	valid_data = np.load(FLAGS.valid_data)
	test_data = np.load(FLAGS.test_data)
	
	train = encode(train_data, -1)
	valid = encode(valid_data, -1)
	test = encode(test_data, 1)
	return train, valid, test

#get_data()
