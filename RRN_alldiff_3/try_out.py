import numpy as np
import tensorflow as tf
from tensorflow.contrib import layers
import get_data 
FLAGS = tf.app.flags.FLAGS

nodes = tf.constant([[[1,1],[2,2],[3,3]],[[4,4],[5,5],[6,6]], [[14,14],[15,15],[16,16]]])
edges = tf.constant([[0,1],[1,2]])
res = tf.gather(nodes, edges, axis=1)
with tf.Session() as sess:
    print(sess.run(res))