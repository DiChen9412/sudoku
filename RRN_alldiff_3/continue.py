from model import MODEL
import time 
import config
import tensorflow as tf
FLAGS = tf.app.flags.FLAGS

def train(model):
    n_updates = 300000
    val_interval = 1000
    model.load(FLAGS.checkpoint_path)
    start = time.time()
    best = float("inf")
    for i in range(n_updates):
        loss = model.train_batch()

        if i % val_interval == 0:
            took = time.time() - start
            print("%05d/%05d %f updates/s %f loss" % (i, n_updates, val_interval / took, loss))
            val_loss = model.val_batch()
            if (val_loss < best):
                best = val_loss
                model.save(i)
            start = time.time()

train(MODEL())
