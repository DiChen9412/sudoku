import tensorflow as tf
import tensorflow.contrib.slim as slim
import numpy as np
import datetime
import config 
from sklearn.metrics import average_precision_score
from sklearn.metrics import roc_auc_score

FLAGS = tf.app.flags.FLAGS
def get_label(data,my_order):

	output = []
	for i in my_order:
		output.append(data[i][1])

	output = np.array(output, dtype="int") 
	return output

def get_feature(data,my_order, offset = None):

	output = []
	for i in my_order:
		output.append(data[i][0])

	output = np.array(output, dtype="int") 
	return output

#####################################################################