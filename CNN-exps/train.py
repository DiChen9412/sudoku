import tensorflow as tf
import tensorflow.contrib.slim as slim
import numpy as np
import datetime
import model
import get_data
import config 
from sklearn.metrics import average_precision_score
from sklearn.metrics import roc_auc_score
from sklearn.metrics import accuracy_score 

FLAGS = tf.app.flags.FLAGS

def MakeSummary(name, value):
	"""Creates a tf.Summary proto with the given name and value."""
	summary = tf.Summary()
	val = summary.value.add()
	val.tag = str(name)
	val.simple_value = float(value)
	return summary

def train_step(sess, hg, merged_summary, summary_writer, input_label, input_feature, train_op, global_step):

	feed_dict={}
	feed_dict[hg.input_feature]=input_feature
	feed_dict[hg.input_label]=input_label
	feed_dict[hg.keep_prob]=1.0

	temp, step, nll_loss, l2_loss, total_loss, summary, indiv_prob= \
	sess.run([train_op, global_step, hg.nll_loss, hg.l2_loss, hg.total_loss,\
	 merged_summary, hg.indiv_prob], feed_dict)

	time_str = datetime.datetime.now().isoformat()
	summary_writer.add_summary(summary,step)

	return indiv_prob, nll_loss, l2_loss, total_loss

def validation_step(sess, hg, data, merged_summary, summary_writer, valid_idx, global_step):

	print('Validating...')

	all_nll_loss = 0
	all_l2_loss = 0
	all_total_loss = 0

	all_indiv_prob = []
	all_label = []
	all_feature = []

	real_batch_size=min(FLAGS.batch_size, len(valid_idx))
	for i in range(int( (len(valid_idx)-1)/real_batch_size )+1):

		start = real_batch_size*i
		end = min(real_batch_size*(i+1), len(valid_idx))

		input_feature = get_data.get_feature(data,valid_idx[start:end])
		input_label = get_data.get_label(data,valid_idx[start:end])

		feed_dict={}
		feed_dict[hg.input_feature]=input_feature
		feed_dict[hg.input_label]=input_label
		feed_dict[hg.keep_prob]=1.0


		nll_loss, l2_loss, total_loss, indiv_prob = sess.run([hg.nll_loss, hg.l2_loss, hg.total_loss, hg.indiv_prob],feed_dict)
	
		all_nll_loss += nll_loss*(end-start)
		all_l2_loss += l2_loss*(end-start)
		all_total_loss += total_loss*(end-start)
	
		for i in indiv_prob:
			all_indiv_prob.append(i)
		for i in input_label:
			all_label.append(i)
		for i in input_feature:
			all_feature.append(i)


	all_indiv_prob = np.array(all_indiv_prob)
	label = np.array(all_label).reshape(-1, 81)
	feature = np.array(all_feature).reshape(-1, 81) 
	#auc = roc_auc_score(all_label,all_indiv_prob)

	nll_loss = all_nll_loss/len(valid_idx)
	l2_loss = all_l2_loss/len(valid_idx)
	total_loss = all_total_loss/len(valid_idx)

	
	pred = np.argmax(all_indiv_prob, axis=2) + 1 # bs, 81 
	target = np.equal(feature, np.zeros_like(feature)).astype(int)
	pred = pred * target
	label = label * target

	digit_acc = 1.0*np.sum(np.equal(pred, label).astype(int))/np.sum(np.greater(label, 0).astype(int))
	puzzle_acc = 1.0*np.sum(np.equal(np.sum(np.equal(pred, label).astype(int), axis=1), 81).astype(int))/pred.shape[0]
	
	time_str = datetime.datetime.now().isoformat()
	#print out the real-time status of the model  
	print("validation results: %s\tdigit_acc=%.6f\t puzzle_acc=%.6f\t nll_loss=%.6f\tl2_loss=%.6f\ttotal_loss=%.6f" % \
		(time_str, digit_acc, puzzle_acc, nll_loss, l2_loss, total_loss))

	current_step = sess.run(global_step) #get the value of global_step
	summary_writer.add_summary(MakeSummary('validation/digit_acc', digit_acc),current_step)
	summary_writer.add_summary(MakeSummary('validation/puzzle_acc', puzzle_acc),current_step)
	summary_writer.add_summary(MakeSummary('validation/nll_loss', nll_loss),current_step)
	return nll_loss

def main(_):

	print('reading npy...')
	np.random.seed(19950420) # set the random seed of numpy 
	data = np.load(FLAGS.data_dir) #load data from the data_dir
	#srd = np.load(FLAGS.srd_dir)
	train_idx = np.load(FLAGS.train_idx) #load the indices of the training set
	valid_idx = np.load(FLAGS.valid_idx) #load the indices of the validation set
	
	one_epoch_iter = len(train_idx) / FLAGS.batch_size # compute the number of iterations in each epoch

	print('reading completed')

	# config the tensorflow
	session_config = tf.ConfigProto()
	session_config.gpu_options.allow_growth = True
	sess = tf.Session(config=session_config)

	print('showing the parameters...\n')

	#parameterList = FLAGS.__dict__['__flags'].items()
	#parameterList = sorted(parameterList)

	# print all the hyper-parameters in the current training
	for key in FLAGS:
		value = FLAGS[key].value
		print("%s\t%s"%(key, value))
	print("\n")


	print('building network...')

	#building the model 
	hg = model.MODEL(is_training=True)

	global_step = tf.Variable(0, name='global_step', trainable=False)

	learning_rate = tf.train.exponential_decay(FLAGS.learning_rate, global_step, (1.0/FLAGS.lr_decay_times)*(FLAGS.max_epoch*one_epoch_iter), FLAGS.lr_decay_ratio, staircase=True)

	#log the learning rate 
	tf.summary.scalar('learning_rate', learning_rate)

	#use the Adam optimizer 
	optimizer = tf.train.AdamOptimizer(learning_rate)

	#set training update ops/backpropagation
	update_ops = tf.get_collection(tf.GraphKeys.UPDATE_OPS)
	with tf.control_dependencies(update_ops):
		train_op = optimizer.minimize(hg.total_loss, global_step = global_step)

	merged_summary = tf.summary.merge_all() # gather all summary nodes together
	summary_writer = tf.summary.FileWriter(FLAGS.summary_dir,sess.graph) #initialize the summary writer

	sess.run(tf.global_variables_initializer()) # initialize the global variables in tensorflow
	saver = tf.train.Saver(max_to_keep=FLAGS.max_keep) #initializae the model saver

	print('building finished')

	#initialize several 
	best_loss = 1e10 
	best_iter = 0

	smooth_nll_loss=0.0
	smooth_l2_loss=0.0
	smooth_total_loss=0.0

	temp_label=[]	
	temp_indiv_prob=[]
	temp_feature = []
	for one_epoch in range(FLAGS.max_epoch):
		
		print('epoch '+str(one_epoch+1)+' starts!')

		np.random.shuffle(train_idx) # random shuffle the training indices  
		
		for i in range(int(len(train_idx)/float(FLAGS.batch_size))):
			
			start = i*FLAGS.batch_size
			end = (i+1)*FLAGS.batch_size

			input_feature = get_data.get_feature(data,train_idx[start:end]) # get the FEATURE features 
			input_label = get_data.get_label(data,train_idx[start:end]) # get the prediction labels 

			#train the model for one step and log the training loss
			indiv_prob, nll_loss, l2_loss, total_loss = train_step(sess, hg, merged_summary, summary_writer, input_label, input_feature, train_op, global_step)
			
			smooth_nll_loss += nll_loss
			smooth_l2_loss += l2_loss
			smooth_total_loss += total_loss
			
			temp_label.append(input_label) #log the labels
			temp_indiv_prob.append(indiv_prob) #log the individual prediction of the probability on each label
			temp_feature.append(input_feature)
			current_step = sess.run(global_step) #get the value of global_step

			if current_step%FLAGS.check_freq==0: #summarize the current training status and print them out

				nll_loss = smooth_nll_loss / float(FLAGS.check_freq)
				l2_loss = smooth_l2_loss / float(FLAGS.check_freq)
				total_loss = smooth_total_loss / float(FLAGS.check_freq)
				
				
				label = np.concatenate(temp_label).reshape(-1, 81)
				indiv_prob = np.concatenate(temp_indiv_prob) #bs, 81, 9
				feature = np.concatenate(temp_feature).reshape(-1, 81)
				#print(label.shape)
				#print(indiv_prob.shape)
				pred = np.argmax(indiv_prob, axis=2) + 1 # bs, 81 
				target = np.equal(feature, np.zeros_like(feature)).astype(int)

				pred = pred * target
				label = label * target


				digit_acc = 1.0*np.sum(np.equal(pred, label).astype(int))/np.sum(np.greater(label, -1).astype(int))
				puzzle_acc = 1.0*np.sum(np.equal(np.sum(np.equal(pred, label).astype(int), axis=1), 81).astype(int))/pred.shape[0]
				

				time_str = datetime.datetime.now().isoformat()
				#print out the real-time status of the model  
				print("%s\tstep=%d\tdigit_acc=%.6f\t puzzle_acc=%.6f\t nll_loss=%.6f\tl2_loss=%.6f\ttotal_loss=%.6f" % \
					(time_str, current_step, digit_acc, puzzle_acc, nll_loss, l2_loss, total_loss))
				#summary_writer.add_summary(MakeSummary('train/auc',auc),current_step)
				
				temp_indiv_prob=[]
				temp_label=[]
				temp_feature = []

				smooth_nll_loss = 0
				smooth_l2_loss = 0
				smooth_total_loss = 0

			if current_step % int(one_epoch_iter*FLAGS.save_epoch)==0: #exam the model on validation set

				#exam the model on validation set
				current_loss = validation_step(sess, hg, data, merged_summary, summary_writer, valid_idx, global_step)

				if current_loss < best_loss: # find a better model than the last checkpoint

					print('current loss:%.10f  which is better than the previous best one!!!'%current_loss)

					best_loss = current_loss
					best_iter = current_step

					print('saving model')
					saved_model_path = saver.save(sess,FLAGS.model_dir+'model',global_step=current_step)

					print('have saved model to ', saved_model_path)
					print("rewriting the number of model to config.py")

					#write the best checkpoint number back to the config.py file
					configFile=open(FLAGS.config_dir, "r")
					content=[line.strip("\n") for line in configFile]
					configFile.close()

					for i in range(len(content)):
						if ("checkpoint_path" in content[i]):
							content[i]="tf.app.flags.DEFINE_string('checkpoint_path', './model/model-%d','The path to a checkpoint from which to fine-tune.')"%best_iter
					
					configFile=open(FLAGS.config_dir, "w")
					for line in content:
						configFile.write(line+"\n")
					configFile.close()

	print('training completed !')
	print('the best loss on validation is '+str(best_loss))
	print('the best checkpoint is '+str(best_iter))
	

if __name__=='__main__':
	tf.app.run()
