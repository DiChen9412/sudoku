import tensorflow as tf
import tensorflow.contrib.slim as slim
import numpy as np
import datetime
import model
import get_data 
import config 
from sklearn.metrics import average_precision_score
from sklearn.metrics import roc_auc_score
import matplotlib.pyplot as plt
import math
import urllib
#from pyheatmap.heatmap import HeatMap
#import seaborn as sns

FLAGS = tf.app.flags.FLAGS
def PuzzleAcc(Qs, As, solus):
    S = 0
    #print(Qs.shape)
    #print(As.shape)
    for k in range(Qs.shape[0]):
        Q = Qs[k].reshape((9,9))
        A = As[k].reshape((9,9))
        solu = solus[k].reshape((9,9))
        n_err = 0
        err_type = []
        for i in range(9):
            for j in range(9):
                if (Q[i][j]!=0 and Q[i][j]!=A[i][j]):
                    n_err += 1
                    err_type.append("type-1: %d %d"%(i,j))

        for i in range(9):
            if (len(list(set(A[i,:])))!=9):
                n_err += 1
                err_type.append("type-2: %d"%(i))

        for i in range(9):    
            if (len(list(set(A[:,i])))!=9):
                n_err += 1 
                err_type.append("type-3: %d"%(i))

        for i in range(3):
            for j in range(3):
                tmp = []
                for k in range(i*3, i*3+3):
                    for l in range(j*3, j*3+3):
                        tmp.append(A[k][l])
                if (len(list(set(tmp)))!=9):
                    n_err += 1
                    err_type.append("type-4: %d %d"%(i,j))
        if (n_err > 0):            
            print(Q)
            print(A)
            print(solu)
            print(n_err, err_type)
        S+= int(n_err == 0)
    return S*1.0/Qs.shape[0]

def main(_):

	print('reading npy...')

	#data = np.load("../data/test_data.npy") 
	#data = np.load("../data/sudoku_17_remove_1.npy") 
	data = np.load(FLAGS.data_dir)
	test_idx = np.load(FLAGS.test_idx)
	test_idx = test_idx[:32]
	#test_idx = np.arange(1280)
	print('reading completed')

	session_config = tf.ConfigProto()
	session_config.gpu_options.allow_growth = True
	sess = tf.Session(config=session_config)

	print('building network...')

	hg = model.MODEL(is_training=False)
	global_step = tf.Variable(0,name='global_step',trainable=False)

	merged_summary = tf.summary.merge_all()
	summary_writer = tf.summary.FileWriter(FLAGS.summary_dir, sess.graph)

	saver = tf.train.Saver(max_to_keep=None)
	saver.restore(sess,FLAGS.checkpoint_path)

	print('restoring from '+FLAGS.checkpoint_path)


	def test_step():
		print('Testing...')
		all_nll_loss = 0
		all_l2_loss = 0
		all_total_loss = 0

		all_indiv_prob = []
		all_label = []
		all_pred = []
		all_feature = []
		sigma=[]
		real_batch_size=min(FLAGS.testing_size, len(test_idx))
		avg_cor=0
		cnt_cor=0
		lim = int( (len(test_idx)-1)/real_batch_size )+1
		for i in range(lim):
			print("%.2f%% completed" % (i*100.0/lim))
			start = real_batch_size*i
			end = min(real_batch_size*(i+1), len(test_idx))

			#input_image= get_data.get_image(images, test_idx[start:end])
			input_feature = get_data.get_feature(data,test_idx[start:end])
			input_label = get_data.get_label(data,test_idx[start:end])
			
			pred_label = np.copy(input_feature.reshape(-1, 81))
			#print(pred_label.shape)
			for step in range(81):
				feed_dict={}
				feed_dict[hg.input_feature]=pred_label.reshape(-1,9,9)
				feed_dict[hg.input_label]=input_label
				feed_dict[hg.keep_prob]=1.0
			
				nll_loss, l2_loss, total_loss, indiv_prob, puzzle_acc= sess.run([hg.nll_loss, hg.l2_loss, \
					hg.total_loss, hg.indiv_prob, hg.puzzle_acc],feed_dict)

				#print("Puzzle_acc =", puzzle_acc)
				for j in range(indiv_prob.shape[0]):
					idx = np.argsort(np.max(indiv_prob[j], axis=1))[::-1] # inverse: max -> min
					#print(idx)
					for k in idx:
						#print(k, pred_label[j][k])
						if (pred_label[j][k] == 0):
							pred_label[j][k] = np.argmax(indiv_prob[j][k]) + 1
							break
				#print(pred_label[0].reshape(9,9))
				#print(input_label[0].reshape(9,9))
				#print()
			

			
			all_nll_loss += nll_loss*(end-start)
			all_l2_loss += l2_loss*(end-start)
			all_total_loss += total_loss*(end-start)


			all_indiv_prob.append(indiv_prob)
			all_label.append(input_label)
			all_pred.append(pred_label)
			all_feature.append(input_feature)
		
		#print("Overall occurrence ratio: %f"%(np.mean(all_label)))
		
		nll_loss = all_nll_loss / len(test_idx)
		l2_loss = all_l2_loss / len(test_idx)
		total_loss = all_total_loss / len(test_idx)

		time_str = datetime.datetime.now().isoformat()

		print("Overall nll_loss=%.6f\tl2_loss=%.6f\ttotal_loss=%.6f \n%s" % (nll_loss, l2_loss, total_loss, time_str))

		pred = np.concatenate(all_pred)
		label = np.concatenate(all_label).reshape(-1, 81)
		feature = np.concatenate(all_feature)

		digit_acc = 1.0*np.sum(np.equal(pred, label).astype(int))/np.sum(np.greater(label, 0).astype(int))
		puzzle_acc = 1.0*np.sum(np.equal(np.sum(np.equal(pred, label).astype(int), axis=1), 81).astype(int))/pred.shape[0]
		puzzle_acc2 = PuzzleAcc(feature, pred, label)
		print("digit_acc = %.4f \t puzzle_acc = %.4f\t puzzle_acc2 = %.4f\t"%(digit_acc, puzzle_acc, puzzle_acc2))
	
	test_step()
	
if __name__=='__main__':
	tf.app.run()



