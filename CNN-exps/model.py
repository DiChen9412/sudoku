import tensorflow as tf
import tensorflow.contrib.slim as slim
import numpy as np
FLAGS = tf.app.flags.FLAGS
def compute_entropy_loss(logits):
    """
    :param logits: (bs, 81, 9)
    """
    P = tf.nn.softmax(logits, axis=2) 
    H = tf.reduce_sum(-P * tf.log(P), axis=2)
    return tf.reduce_mean(H)

def compute_alldiff_loss(logits, edges, keep_prob):
    """
    :param logits: (bs, 81, 9)
    :param edges: (27, 9)
    """
    P = tf.nn.softmax(logits, axis=2)

    all_diffs = tf.gather(P, edges, axis=1) # bs*27*9*9
    all_diffs_P = tf.reduce_mean(all_diffs, axis=2)
    H = tf.reduce_sum(-all_diffs_P * tf.log(all_diffs_P), axis=2) #bs*27
    H = slim.dropout(H, keep_prob = keep_prob)
    return -tf.reduce_mean(H)

def sudoku_alldiff(): #produce all diff sets
    idx = np.arange(81).reshape(9, 9)
    all_diff = []
    for i in range(9):
        all_diff.append(idx[:,i])

    for i in range(9):
        all_diff.append(idx[i,:])

    for i in range(3):
        for j in range(3):
            all_diff.append(idx[i*3:(i+1)*3, j*3:(j+1)*3].reshape(-1))
    all_diff = np.asarray(all_diff, dtype="int32")
    #print(all_diff.shape)
    return all_diff #(1620, 2)

class MODEL:

    def __init__(self,is_training):

        tf.set_random_seed(19950420)

        r_dim = FLAGS.r_dim
        
        self.input_feature = tf.placeholder(dtype=tf.int32,shape=[None, 9, 9],name='input_feature')
        
        self.input_label = tf.placeholder(dtype=tf.int32,shape=[None,9, 9],name='input_label')

        self.keep_prob = tf.placeholder(tf.float32) #keep probability for the dropout

        weights_regularizer = slim.l2_regularizer(FLAGS.weight_decay)

        ############## compute mu & sigma ###############
        Q = tf.reshape(self.input_feature, [-1, 81])
        A = tf.reshape(self.input_label, [-1, 81])
        target = tf.to_float(tf.equal(Q, 0))

        batch_norm = slim.batch_norm

        batch_norm_params = {'is_training':is_training,'updates_collections':tf.GraphKeys.UPDATE_OPS,'decay':0.99,'epsilon':0.00001}

        ############## CNN ###############
        x = tf.to_float(tf.reshape(Q, [-1, 9, 9, 1]))
        for i in range(10):
            x = slim.conv2d(inputs=x, num_outputs=512, kernel_size=[3,3],stride=[1,1], normalizer_fn=slim.batch_norm, normalizer_params=batch_norm_params,weights_regularizer = weights_regularizer)
            print(x.shape)
 
        
        self.logits = slim.conv2d(inputs=x, num_outputs=9, kernel_size=[3,3],stride=[1,1], normalizer_fn=slim.batch_norm, normalizer_params=batch_norm_params,weights_regularizer = weights_regularizer)
        print(self.logits.shape)

        #x = slim.fully_connected(x, 1024, weights_regularizer=weights_regularizer, scope='encoder/fc_1')
        #x = slim.fully_connected(x, 1024, weights_regularizer=weights_regularizer, scope='encoder/fc_2')


        #dropout
        #feature1 = slim.dropout(self.fc_3, keep_prob=self.keep_prob, is_training=is_training)

        #feature = x 
        #self.logits = slim.fully_connected(feature, 81*9, weights_regularizer=weights_regularizer, activation_fn = None, scope='generator/logits')
        self.logits = tf.reshape(self.logits, [-1, 81, 9])

        self.indiv_prob=tf.nn.softmax(self.logits, axis=2)

        self.nll_loss=tf.reduce_mean(tf.nn.sparse_softmax_cross_entropy_with_logits(labels=A-1,logits=self.logits))
        
        alldiff_constraints = sudoku_alldiff()
        self.entropy_loss = compute_entropy_loss(self.logits)      
        self.all_diff_loss = compute_alldiff_loss(self.logits, alldiff_constraints, self.keep_prob)
        self.same_loss = tf.reduce_sum(tf.nn.sparse_softmax_cross_entropy_with_logits(labels=A-1,logits=self.logits)*(1-target))/tf.reduce_sum(1 - target)
        self.sci_loss = 0.001*self.entropy_loss + self.all_diff_loss + 0.1*self.same_loss
        
        self.pred = tf.cast(tf.argmax(self.logits, axis=2)+1, tf.int32) #bs, 81

        equal = tf.equal(A, self.pred)  #bs, 81 bool 
        self.digit_acc = tf.reduce_mean(tf.to_float(equal))
        tf.summary.scalar('digit-acc', self.digit_acc)
        self.puzzle_acc = tf.reduce_mean(tf.to_float(tf.reduce_all(equal, axis=1))) # logitcal and 
        tf.summary.scalar('puzzle-acc', self.puzzle_acc)
        ###### loss ##############
        tf.summary.scalar('nll_loss',self.nll_loss)

        self.l2_loss = tf.add_n(tf.losses.get_regularization_losses())#+FLAGS.weight_decay*tf.nn.l2_loss(self.r_sqrt_sigma)
        tf.summary.scalar('l2_loss',self.l2_loss)
        
        self.total_loss = self.l2_loss + self.nll_loss #+ self.all_diff_loss (this is for TTCI)
        tf.summary.scalar('total_loss',self.total_loss)

        
