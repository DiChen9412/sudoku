import tensorflow as tf
import tensorflow.contrib.slim as slim
import numpy as np
import datetime
import model
import get_data 
import config 
from sklearn.metrics import average_precision_score
from sklearn.metrics import roc_auc_score
import matplotlib.pyplot as plt
import math
import urllib
#from pyheatmap.heatmap import HeatMap
#import seaborn as sns

FLAGS = tf.app.flags.FLAGS
def PuzzleAcc(Qs, As, solus):
    S = 0
    #print(Qs.shape)
    #print(As.shape)
    for k in range(Qs.shape[0]):
        Q = Qs[k].reshape((9,9))
        A = As[k].reshape((9,9))
        solu = solus[k].reshape((9,9))
        n_err = 0
        err_type = []
        for i in range(9):
            for j in range(9):
                if (Q[i][j]!=0 and Q[i][j]!=A[i][j]):
                    n_err += 1
                    err_type.append("type-1: %d %d"%(i,j))

        for i in range(9):
            if (len(list(set(A[i,:])))!=9):
                n_err += 1
                err_type.append("type-2: %d"%(i))

        for i in range(9):    
            if (len(list(set(A[:,i])))!=9):
                n_err += 1 
                err_type.append("type-3: %d"%(i))

        for i in range(3):
            for j in range(3):
                tmp = []
                for k in range(i*3, i*3+3):
                    for l in range(j*3, j*3+3):
                        tmp.append(A[k][l])
                if (len(list(set(tmp)))!=9):
                    n_err += 1
                    err_type.append("type-4: %d %d"%(i,j))
        if (n_err > 0):            
            print(Q)
            print(A)
            print(solu)
            print(n_err, err_type)
        S+= int(n_err == 0)
    return S*1.0/Qs.shape[0]

def main(_):

	print('reading npy...')

	#data = np.load("../data/test_data.npy") 
	#data = np.load("../data/sudoku_17_remove_1.npy") 
	data = np.load(FLAGS.data_dir)
	test_idx = np.load(FLAGS.test_idx)
	test_idx = test_idx
	#test_idx = np.arange(1280)
	print('reading completed')

	# config the tensorflow
	session_config = tf.ConfigProto(allow_soft_placement=True)
	session_config.gpu_options.allow_growth = True
	sess = tf.Session(config=session_config)

	print('showing the parameters...\n')

	#parameterList = FLAGS.__dict__['__flags'].items()
	#parameterList = sorted(parameterList)

	# print all the hyper-parameters in the current training
	for key in FLAGS:
		value = FLAGS[key].value
		print("%s\t%s"%(key, value))
	print("\n")


	print('building network...')

	#building the model 
	
	global_step = tf.Variable(0, name='global_step', trainable=False)

	learning_rate = FLAGS.learning_rate
	#log the learning rate 
	tf.summary.scalar('learning_rate', learning_rate)

	#use the Adam optimizer 
	optimizer = tf.train.AdamOptimizer(learning_rate)

	hg = model.MODEL(is_training=True, optimizer = optimizer, global_step = global_step)

	merged_summary = tf.summary.merge_all()
	summary_writer = tf.summary.FileWriter(FLAGS.summary_dir, sess.graph)

	saver = tf.train.Saver(max_to_keep=None)
	saver.restore(sess,FLAGS.checkpoint_path)

	print('restoring from '+FLAGS.checkpoint_path)


	def test_step():
		print('Testing...')
		all_puzzle_acc = 0
		all_pred = []
		all_feature = []
		all_label = []

		real_batch_size=FLAGS.batch_size
		lim = int(len(test_idx)/real_batch_size)
		for i in range(lim):
			print("%.2f%% completed" % (i*100.0/lim))
			start = real_batch_size*i
			end = real_batch_size*(i+1)

			input_feature = get_data.get_feature(data,test_idx[start:end])
			input_label = get_data.get_label(data,test_idx[start:end])
			
			feed_dict={}
			feed_dict[hg.input_feature]=input_feature
			feed_dict[hg.input_label]=input_label
			feed_dict[hg.keep_prob]=1.0
			
			for j in range(1000): #SCI
	
				__, pred, sci_loss, alldiff_loss, entropy_loss, same_loss= \
				sess.run([hg.sci_op, hg.pred, hg.sci_loss, hg.alldiff_loss, hg.entropy_loss, hg.same_loss],feed_dict)
				puzzle_acc = PuzzleAcc(input_feature, pred, input_label)

				print("sci_loss = %.6f alldiff_loss = %.6f entropy_loss = %.6f same_loss = %.6f puzzle_acc=%.6f"%\
					(sci_loss, alldiff_loss, entropy_loss, same_loss, puzzle_acc))
				if (puzzle_acc == 1):
					break
				
			all_puzzle_acc += puzzle_acc
			all_pred.append(pred)
			all_feature.append(input_feature)
			all_label.append(input_label)
		#print("Overall occurrence ratio: %f"%(np.mean(all_label)))
	
		puzzle_acc = all_puzzle_acc / lim

		time_str = datetime.datetime.now().isoformat()

		
		pred = np.concatenate(all_pred)
		label = np.concatenate(all_label).reshape(-1, 81)
		feature = np.concatenate(all_feature)

		puzzle_acc2 = PuzzleAcc(feature, pred, label)

		print("Overall puzzle_acc=%.6f\t" % (puzzle_acc))
	
	test_step()
	
if __name__=='__main__':
	tf.app.run()



