import tensorflow as tf
FLAGS = tf.app.flags.FLAGS
tf.app.flags.DEFINE_string('model_dir', './model/','path to store the checkpoints of the model')
tf.app.flags.DEFINE_string('summary_dir', './summary/','path to store analysis summaries used for tensorboard')
tf.app.flags.DEFINE_string('train_summary_dir', './summary/train','path to store summary_dir for training')
tf.app.flags.DEFINE_string('valid_summary_dir', './summary/valid','path to store summary_dir for validation')
tf.app.flags.DEFINE_string('test_summary_dir', './summary/test','path to store summary_dir for test')
tf.app.flags.DEFINE_string('config_dir', './config.py','path to config.py')
tf.app.flags.DEFINE_string('checkpoint_path', './model/model-2000','The path to a checkpoint from which to fine-tune.')
tf.app.flags.DEFINE_string('visual_dir', './visualization/','path to store visualization codes and data')

tf.app.flags.DEFINE_string('data_dir', '../data/data_easy.npy','The path of input observation data')
#tf.app.flags.DEFINE_string('srd_dir', '../data/loc_ny_nlcd2006.npy','The path of input srd data')
tf.app.flags.DEFINE_string('train_idx', '../data/train_idx.npy','The path of training data index')
tf.app.flags.DEFINE_string('valid_idx', '../data/valid_idx.npy','The path of validation data index')
tf.app.flags.DEFINE_string('test_idx', '../data/test_idx.npy','The path of testing data index')

tf.app.flags.DEFINE_integer('batch_size', 32, 'the number of data points in one minibatch') #128
tf.app.flags.DEFINE_integer('testing_size', 32, 'the number of data points in one testing or validation batch') #128
tf.app.flags.DEFINE_float('learning_rate', 0.001, 'initial learning rate')
#tf.app.flags.DEFINE_float('pred_lr', 1, 'the learning rate for predictor')
tf.app.flags.DEFINE_integer('max_epoch', 200, 'max epoch to train')
tf.app.flags.DEFINE_float('weight_decay', 0.000001, 'weight decay rate')
tf.app.flags.DEFINE_float('threshold', 0.5, 'The probability threshold for the prediction')
tf.app.flags.DEFINE_float('lr_decay_ratio', 0.5, 'The decay ratio of learning rate')
tf.app.flags.DEFINE_float('lr_decay_times', 200.0, 'How many times does learning rate decay')
tf.app.flags.DEFINE_integer('n_test_sample', 10000, 'The sampling times for the testing')
tf.app.flags.DEFINE_integer('n_train_sample', 100, 'The sampling times for the training') #100
tf.app.flags.DEFINE_integer('n_infer_step', 32, '# the steps for LSTM ') #100
tf.app.flags.DEFINE_integer('emb_size', 16, 'The embedding size for positions, digits, etc') #100
tf.app.flags.DEFINE_integer('n_hidden', 128, 'The hidden layer size for MLP and LSTM') #10


tf.app.flags.DEFINE_float('save_epoch', 1.0, 'epochs to save the checkpoint of the model')
tf.app.flags.DEFINE_integer('max_keep', 3, 'maximum number of saved model')
tf.app.flags.DEFINE_integer('check_freq', 10, 'checking frequency')



